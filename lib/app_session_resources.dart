import 'package:fluttertoast/fluttertoast.dart';
import 'package:red_dragon/app_settings_api.dart';
import 'package:red_dragon/classes/OrderCity.dart';
import 'package:red_dragon/classes/cart_product.dart';
import 'package:red_dragon/classes/category.dart';
import 'package:red_dragon/classes/contacts.dart';
import 'package:red_dragon/classes/order_settings.dart';
import 'package:red_dragon/classes/product.dart';
import 'package:red_dragon/classes/user.dart';
import 'package:red_dragon/pages/menu/menu_api.dart';
import 'package:red_dragon/pages/profile/user_api.dart';
import 'package:red_dragon/classes/promotion.dart';
import 'package:red_dragon/pages/promotions/promotions_api.dart';
import 'package:red_dragon/pages/auth/auth_repository.dart';

class AppSessionResources {
  static User user;
  static String token;
  static List<Promotion> promotions = [];
  static List<Product> additionalProducts = [];
  static Contacts contacts;
  static OrderSettings orderSettings;
  static List<Category> categories = [];
  static List<OrderCity> cities = [];
  static Map<int, List<Product>> _products = {};

  static void clear() {
    user = null;
    token = null;
    _products = {};
  }

  static Future<void> init() async {
    List<dynamic> data = await Future.wait([
      PromotionsApi.getPromotions(),
      AppSettingsApi.getContacts(),
      AppSettingsApi.getOrderSettings(),
      MenuApi.getCategories(),
      AppSettingsApi.getCities(),
      MenuApi.getAdditionalProducts(),
    ]);
    promotions = data[0];
    contacts = data[1];
    orderSettings = data[2];
    categories = data[3];
    cities = data[4];
    additionalProducts = data[5];

    final isAuth = await AuthRepository.isAuth();
    if (isAuth) {
      final token = await AuthRepository.getToken();
      AppSessionResources.token = token;
      user = await UserApi.getUser();
    }
  }

  static Future<List<Product>> getProducts(Category category) async {
    for (int i = 0; i < _products.keys.length; i++) {
      if (_products.keys.toList()[i] == category.id) {
        return _products[category.id];
      }
    }
    List<Product> products = await MenuApi.getProducts(category.id);
    _products[category.id] = products;
    return products;
  }

  static OrderCity tryFindCity(String name) {
    return cities.firstWhere((element) => element.name == name, orElse: null);
  }

  static bool isOrderWorkingNow() {
    final now = DateTime.now();
    final today = DateTime(now.year, now.month, now.day);

    final fromStringAsHours = orderSettings.workFrom.substring(0, 2);
    final fromIntAsHours = int.tryParse(fromStringAsHours);

    final fromStringAsMin = orderSettings.workFrom.substring(3, 5);
    final fromIntAsMin = int.tryParse(fromStringAsMin);

    final fromTime = today.add(Duration(hours: fromIntAsHours, minutes: fromIntAsMin));

    final toStringAsHours = orderSettings.workTo.substring(0, 2);
    final toIntAsHours = int.tryParse(toStringAsHours);

    final toStringAsMin = orderSettings.workTo.substring(3, 5);
    final toIntAsMin = int.tryParse(toStringAsMin);

    var additional = 0;
    if (toIntAsHours < fromIntAsHours) {
      additional = 24;
    }
    final toTime = today.add(Duration(hours: toIntAsHours + additional, minutes: toIntAsMin));

    return now.isAfter(fromTime) && now.isBefore(toTime);
  }
}
