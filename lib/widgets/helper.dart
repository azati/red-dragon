import 'package:flutter/material.dart';

Future<bool> showLoadingAlert(BuildContext context) {
  Widget content = WillPopScope(
    child: Container(
      color: Colors.transparent,
      child: Center(
        child: Container(
          width: 100,
          height: 100,
          child: CircularProgressIndicator(),
        ),
      ),
    ),
    onWillPop: () {},
  );

  return showDialog(
        context: context,
        builder: (context) => content,
      ) ??
      false;
}

Future<bool> showAlert(BuildContext context, String title, String message) {
  return showDialog(
    context: context,
    builder: (context) => AlertDialog(
      title: Text(title),
      content: Text(message),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text(
            'Ок',
            style: TextStyle(color: Colors.red),
          ),
        ),
      ],
    ),
  );
}
