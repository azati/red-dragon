import 'package:flutter/material.dart';
import 'package:red_dragon/app_session_resources.dart';
import 'package:red_dragon/pages/order_accepted/order_accepted_page.dart';
import 'package:url_launcher/url_launcher.dart';

class PhoneButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
      child: RawMaterialButton(
        onPressed: () {
          final formatterPhone = AppSessionResources.contacts.phone;
          final uri = "tel:$formatterPhone";
          launch(uri);
        },
        constraints: BoxConstraints(),
        elevation: 2.0,
        fillColor: Colors.red[50],
        child: Icon(
          Icons.call,
          size: 18.0,
          color: Colors.red[600],
        ),
        padding: EdgeInsets.all(8.0),
        shape: CircleBorder(),
      ),
    );
  }
}
