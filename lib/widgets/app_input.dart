import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppInput extends StatelessWidget {
  final TextEditingController controller;
  final String hint;
  final bool editable;
  final int width;
  final List<TextInputFormatter> format;

  const AppInput({
    Key key,
    @required this.controller,
    @required this.hint,
    this.editable = true,
    this.width = 100,
    this.format = const [],
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textField = TextField(
      controller: controller,
      cursorColor: Colors.red[600],
      decoration: InputDecoration(
        enabled: editable,
        hintText: hint,
        fillColor: Colors.white,
        filled: true,
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey[300]),
          borderRadius: BorderRadius.circular(25.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.red[600]),
          borderRadius: BorderRadius.circular(25.0),
        ),
      ),
      inputFormatters: format,
    );

    if (width == null) {
      return Container(
        height: 50,
        child: textField,
      );
    } else {
      return Container(
        height: 50,
        width: 100,
        child: textField,
      );
    }
  }

  factory AppInput.disabled({String hint}) {
    return AppInput(
      controller: null,
      hint: hint,
      editable: false,
      width: null,
    );
  }
}
