import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:red_dragon/widgets/phone_button.dart';

class AppScaffold extends StatelessWidget {
  final Widget body;
  final Widget bottomNavigationBar;
  final String appBarTitle;
  final Widget pinnedBottomWidget;

  const AppScaffold({
    Key key,
    @required this.body,
    this.appBarTitle,
    this.bottomNavigationBar,
    this.pinnedBottomWidget,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarTitle != null
          ? AppBar(
              brightness: Brightness.light,
              elevation: 2.0,
              leading: InkWell(
                child: Icon(
                  Icons.arrow_back_ios,
                  size: 16,
                  color: Colors.black,
                ),
                onTap: () {
                  Navigator.of(context).pop();
                },
              ),
              actions: [
                PhoneButton(),
              ],
              title: Text(
                appBarTitle,
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  fontFamily: 'PTSans',
                ),
              ),
              backgroundColor: Colors.white,
            )
          : PreferredSize(
              child: Container(),
              preferredSize: Size(0, 0),
            ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(image: AssetImage('assets/background.png'), fit: BoxFit.cover),
        ),
        child: Column(
          children: [
            Expanded(
              child: Align(
                alignment: Alignment.topCenter,
                child: SingleChildScrollView(
                  primary: false,
                  child: body,
                ),
              ),
            ),
            pinnedBottomWidget != null ? pinnedBottomWidget : Container()
          ],
        ),
      ),
      bottomNavigationBar: bottomNavigationBar,
    );
  }
}
