import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppInputWithSuffix extends StatelessWidget {
  final TextEditingController controller;
  final String hint;
  final List<TextInputFormatter> inputFormatters;
  final Function onTap;

  const AppInputWithSuffix({
    Key key,
    @required this.controller,
    @required this.hint,
    this.inputFormatters,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Expanded(
          child: TextField(
            controller: controller,
            cursorColor: Colors.red[600],
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(
                left: 16,
                right: 4,
              ),
              hintText: hint,
              fillColor: Colors.white,
              filled: true,
              border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey[300]),
                borderRadius: BorderRadius.horizontal(
                  left: Radius.circular(25),
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.red[600]),
                borderRadius: BorderRadius.horizontal(
                  left: Radius.circular(25),
                ),
              ),
            ),
            inputFormatters: inputFormatters,
          ),
        ),
        Container(
          height: 48,
          width: 44,
          decoration: BoxDecoration(
            color: Color(0xFFE22E27),
            borderRadius: BorderRadius.horizontal(
              right: Radius.circular(25),
            ),
          ),
          child: InkWell(
            child: Icon(
              Icons.done,
              color: Colors.white,
            ),
            onTap: () {
              onTap?.call();
            },
          ),
        )
      ],
    );
  }
}
