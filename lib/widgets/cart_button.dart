import 'package:flutter/material.dart';

class CartButton extends StatelessWidget {

  final Function onTap;

  const CartButton({Key key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: () {
        onTap();
      },
      constraints: BoxConstraints(),
      elevation: 2.0,
      fillColor: Colors.white,
      child: Icon(
        Icons.shopping_cart,
        size: 15.0,
        color: Colors.red[600],
      ),
      padding: EdgeInsets.all(8.0),
      shape: CircleBorder(),
    );
  }
}
