import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:red_dragon/classes/OrderCity.dart';
import 'package:red_dragon/classes/contacts.dart';
import 'package:red_dragon/classes/order_settings.dart';

class AppSettingsApi {
  static Future<Contacts> getContacts() async {
    final endpoint = 'https://rd-sushi.ru/api/settings/index';
    final response = await http.get(
      endpoint,
    );
    if (response.statusCode == 200) {
      dynamic json = jsonDecode(response.body);
      return Contacts.fromJson(json);
    } else {
      throw Exception('getContacts response code is not 200');
    }
  }

  static Future<OrderSettings> getOrderSettings() async {
    final endpoint = 'https://rd-sushi.ru/api/settings/view';
    final response = await http.get(
      endpoint,
    );
    if (response.statusCode == 200) {
      dynamic json = jsonDecode(response.body);
      return OrderSettings.fromJson(json);
    } else {
      throw Exception('getOrderSettings response code is not 200');
    }
  }

  static Future<List<OrderCity>> getCities() async {
    final endpoint = 'https://rd-sushi.ru/api/settings/order-city';
    final response = await http.get(
      endpoint,
    );
    if (response.statusCode == 200) {
      List json = jsonDecode(response.body);
      List<OrderCity> cities = [];
      for (int i = 0; i < json.length; i++) {
        cities.add(OrderCity.fromJson(json[i]));
      }
      return cities;
    } else {
      throw Exception('getOrderSettings response code is not 200');
    }
  }
}
