import 'package:flutter/material.dart';
import 'package:red_dragon/app_session_resources.dart';
import 'package:red_dragon/pages/main/main_page.dart';
import 'package:red_dragon/pages/splash_screen/splash_screen.dart';
import 'package:flutter/services.dart';

class AppView extends StatefulWidget {
  @override
  _AppViewState createState() => _AppViewState();
}

class _AppViewState extends State<AppView> {
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    AppSessionResources.init().then((_) => _onSuccess()).catchError((e) => _onError(e));
  }

  void _onSuccess() {
    setState(() {
      isLoading = false;
    });
  }

  void _onError(Exception exception) {
    final snackBar = SnackBar(content: Text(exception.toString()));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      home: isLoading ? SplashPage() : MainPage(),
      theme: ThemeData(
        accentColor: Colors.red,
        textSelectionHandleColor: Colors.red,
      ),
    );
  }
}