import 'package:flutter/cupertino.dart';

class Contacts {
  final String orderEmail;
  final String chainEmail;
  final String address;
  final String phone;
  final String instagramUrl;

  Contacts({
    @required this.orderEmail,
    @required this.chainEmail,
    @required this.address,
    @required this.phone,
    @required this.instagramUrl,
  });

  factory Contacts.fromJson(dynamic json) {
    return Contacts(
        orderEmail: json['order_email'],
        chainEmail: json['chain_email'],
        address: json['address'],
        phone: json['phone'],
        instagramUrl: json['instagram']);
  }
}
