import 'package:flutter/cupertino.dart';

class Promotion {
  final int id;
  final String title;
  final String description;
  final String imageUrl;
  final String code;
  final DateTime workUntil;

  Promotion({
    @required this.id,
    @required this.title,
    @required this.description,
    @required this.imageUrl,
    @required this.code,
    @required this.workUntil,
  });

  factory Promotion.fromJson(dynamic json) {
    return Promotion(
      id: json['id'],
      title: json['name'],
      description: json['description'],
      imageUrl: "https://rd-sushi.ru/frontend/web/images/stock/${json['img']}",
      code: json['code'],
      workUntil: DateTime.fromMillisecondsSinceEpoch(json['date'] * 1000),
    );
  }

  factory Promotion.empty() {
    return Promotion(
      id: null,
      title: null,
      description: null,
      imageUrl: null,
      code: null,
      workUntil: null,
    );
  }
}
