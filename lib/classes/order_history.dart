import 'package:flutter/material.dart';
import 'package:red_dragon/classes/order_history_product.dart';

class OrderHistory {
  final int id;
  final String phone;
  final int deliverySum;
  final int quantity;
  final String text;
  final int paymentMethod;
  final int orderSum;
  final String promoCode;
  final int bonusSum;
  final String city;
  final String address;
  final String office;
  final String entrance;
  final String doorphone;
  final String floor;
  final int type;
  final String fio;
  final DateTime date;
  final List<OrderHistoryProduct> products;

  OrderHistory({
    @required this.id,
    @required this.deliverySum,
    @required this.quantity,
    @required this.text,
    @required this.paymentMethod,
    @required this.orderSum,
    @required this.promoCode,
    @required this.bonusSum,
    @required this.city,
    @required this.office,
    @required this.entrance,
    @required this.doorphone,
    @required this.floor,
    @required this.type,
    @required this.fio,
    @required this.products,
    @required this.address,
    @required this.phone,
    @required this.date,
  });

  factory OrderHistory.fromJson(dynamic json) {
    return OrderHistory(
      id: json['id'],
      phone: json['phone'],
      deliverySum: json['delivery_sum'],
      quantity: json['quantity'],
      text: json['text'],
      paymentMethod: json['payment_method'],
      orderSum: json['order_sum'],
      promoCode: json['promo_code'],
      bonusSum: json['bonus_sum'],
      city: json['city'],
      address: json['address'],
      office: json['office'],
      entrance: json['entrance'],
      doorphone: json['doorphone'],
      floor: json['floor'],
      type: json['type'],
      fio: json['fio'],
      date: DateTime.fromMillisecondsSinceEpoch(json['created_at'] * 1000),
      products: _getProducts(json['items']),
    );
  }

  static List<OrderHistoryProduct> _getProducts(dynamic items) {
    List<OrderHistoryProduct> products = [];
    for (int i = 0; i < items.length; i++) {
      products.add(OrderHistoryProduct.fromJson(items[i]));
    }
    return products;
  }
}
