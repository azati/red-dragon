enum PaymentType {
  CASH,
  CARD,
  APPLE_PAY,
}

String getPaymentTypeTitle(PaymentType paymentType) {
  if (paymentType == PaymentType.CASH) {
    return 'Наличными';
  } else if (paymentType == PaymentType.CARD) {
    return 'Картой';
  } else {
    return 'Apple Pay';
  }
}
