import 'package:flutter/cupertino.dart';

class OrderResponse {
  final bool success;

  OrderResponse({
    @required this.success,
  });

  factory OrderResponse.fromJson(dynamic json) {
    return OrderResponse(
      success: true,
    );
  }
}
