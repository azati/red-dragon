import 'package:flutter/cupertino.dart';
import 'package:red_dragon/classes/product.dart';

class CartProduct {
  int quantity = 0;
  Product product;

  CartProduct({
    @required this.product,
    @required this.quantity,
  });
}
