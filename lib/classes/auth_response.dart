import 'package:flutter/cupertino.dart';
import 'package:red_dragon/classes/user.dart';

class AuthResponse {
  final bool success;
  final User user;

  AuthResponse({
    @required this.success,
    @required this.user,
  });

  factory AuthResponse.fromJson(dynamic json) {
    return AuthResponse(
      success: true,
      user: User.fromJson(json),
    );
  }

  factory AuthResponse.empty() {
    return AuthResponse(
      success: false,
      user: null,
    );
  }
}
