import 'package:flutter/cupertino.dart';
import 'package:red_dragon/classes/order_product.dart';

class OrderDetails {
  final String orderCity;
  final String orderAddress;
  final String orderOffice;
  final String orderEntrance;
  final String orderDoorPhone;
  final String orderFloor;

  final String comment;

  // Наличные - "0", "Картой - "1"
  final int paymentType;
  final DateTime orderReadyTime;
  final String promocode;
  final int usedBonuses;
  final int deliveryPrice;
  final int productsPrice;
  final int finalPrice;
  final List<OrderProduct> products;

  // Доставка - "1", Самовывоз - "0".
  final int orderType;

  OrderDetails({
    @required this.orderCity,
    @required this.orderAddress,
    @required this.orderOffice,
    @required this.orderEntrance,
    @required this.orderDoorPhone,
    @required this.orderFloor,
    @required this.comment,
    @required this.paymentType,
    @required this.orderReadyTime,
    @required this.promocode,
    @required this.usedBonuses,
    @required this.deliveryPrice,
    @required this.productsPrice,
    @required this.finalPrice,
    @required this.products,
    @required this.orderType,
  });

  factory OrderDetails.formData({
    @required String orderCity,
    @required String orderAddress,
    @required String orderOffice,
    @required String orderEntrance,
    @required String orderDoorPhone,
    @required String orderFloor,
    @required String comment,
    @required int paymentType,
    @required DateTime orderReadyTime,
    @required String promocode,
    @required int usedBonuses,
    @required int deliveryPrice,
    @required int productsPrice,
    @required int finalPrice,
    @required List<OrderProduct> products,
    @required int orderType,
  }) {
    return OrderDetails(
      orderCity: orderCity,
      orderAddress: orderAddress,
      orderOffice: orderOffice,
      orderEntrance: orderEntrance,
      orderDoorPhone: orderDoorPhone,
      orderFloor: orderFloor,
      comment: comment,
      paymentType: paymentType,
      orderReadyTime: orderReadyTime,
      promocode: promocode,
      usedBonuses: usedBonuses,
      deliveryPrice: deliveryPrice,
      productsPrice: productsPrice,
      finalPrice: finalPrice,
      products: products,
      orderType: orderType,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'orderCity': orderCity,
      'orderAddress': orderAddress,
      'orderOffice': orderOffice,
      'orderEntrance': orderEntrance,
      'orderDoorPhone': orderDoorPhone,
      'orderFloor': orderFloor,
      'comment': comment,
      'paymentType': paymentType,
      'orderReadyTime': orderReadyTime.toUtc().millisecondsSinceEpoch,
      'promocode': promocode,
      'usedBonuses': usedBonuses,
      'deliveryPrice': deliveryPrice,
      'productsPrice': productsPrice,
      'finalPrice': finalPrice,
      'products': products,
      'orderType': orderType,
    };
  }
}
