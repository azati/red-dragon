import 'package:flutter/cupertino.dart';

class Category {
  final int id;
  final String name;
  final int sort;

  Category({
    @required this.sort,
    @required this.id,
    @required this.name,
  });

  factory Category.fromJson(dynamic json) {
    return Category(
      id: json['id'],
      name: json['name'],
      sort: json['sort'],
    );
  }
}
