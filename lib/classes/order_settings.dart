import 'package:flutter/cupertino.dart';

class OrderSettings {
  final String workFrom;
  final String workTo;
  final int minSum;

  OrderSettings({
    @required this.workFrom,
    @required this.workTo,
    @required this.minSum,
  });

  factory OrderSettings.fromJson(dynamic json) {
    return OrderSettings(
      workFrom: json['work_from'],
      workTo: json['work_to'],
      minSum: json['min_sum'],
    );
  }
}
