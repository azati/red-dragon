import 'package:flutter/cupertino.dart';

class Product {
  final int id;
  final String name;
  final int categoryId;
  final String imageUrl;
  final String description;
  final int price;
  final int weight;
  final int calorie;
  final int quantity;
  final int additionaly;

  Product({
    @required this.id,
    @required this.name,
    @required this.categoryId,
    @required this.description,
    @required this.price,
    @required this.imageUrl,
    @required this.weight,
    @required this.calorie,
    @required this.quantity,
    @required this.additionaly,
  });

  factory Product.fromJson(dynamic json) {
    return Product(
      id: json['id'],
      name: json['name'],
      categoryId: json['category_id'],
      description: json['description'],
      price: json['price'],
      weight: json['weight'],
      calorie: json['calorie'],
      quantity: json['quantity'],
      additionaly: json['additionally'],
      imageUrl: "https://rd-sushi.ru/frontend/web/images/product/${json['img']}"
    );
  }
}
