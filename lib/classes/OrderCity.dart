import 'package:flutter/material.dart';

class OrderCity {
  final int id;
  final String name;
  final int deliveryPrice;
  final int freeDeliveryPrice;

  OrderCity({
    @required this.id,
    @required this.name,
    @required this.deliveryPrice,
    @required this.freeDeliveryPrice,
  });

  factory OrderCity.fromJson(dynamic json) {
    return OrderCity(
      id: json['id'],
      name: json['city'],
      deliveryPrice: json['price'],
      freeDeliveryPrice: json['to_free'],
    );
  }
}
