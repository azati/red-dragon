import 'package:flutter/material.dart';

class OrderHistoryProduct {
  final int id;
  final int orderId;
  final int productId;
  final int quantity;
  final int sum;
  final int price;
  final String name;

  OrderHistoryProduct({
    @required this.name,
    @required this.id,
    @required this.orderId,
    @required this.productId,
    @required this.quantity,
    @required this.sum,
    @required this.price,
  });

  factory OrderHistoryProduct.fromJson(dynamic json) {
    return OrderHistoryProduct(
      id: json['id'],
      orderId: json['order_id'],
      productId: json['product_id'],
      quantity: json['qty_item'],
      sum: json['sum_item'],
      price: json['price'],
      name: json['product_name'],
    );
  }
}
