class OrderProduct {
  final int productId;
  final int quantity;

  OrderProduct({
    this.productId,
    this.quantity,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': productId,
      'quantity': quantity,
    };
  }
}
