import 'package:flutter/material.dart';

class User {
  final int id;
  final String accessToken;
  final String email;
  final String fio;
  final String phone;
  final String city;
  final String address;
  final String office;
  final String entrance;
  final String doorPhone;
  final String floor;
  final int bonusSum;
  final int bonusPercent;
  final int orders;

  User({
    @required this.id,
    @required this.accessToken,
    @required this.email,
    @required this.fio,
    @required this.phone,
    @required this.city,
    @required this.address,
    @required this.office,
    @required this.entrance,
    @required this.doorPhone,
    @required this.floor,
    @required this.bonusSum,
    @required this.bonusPercent,
    @required this.orders,
  });

  factory User.fromJson(dynamic json) {
    return User(
      id: json['id'],
      accessToken: json['access_token'],
      email: json['email'],
      fio: json['fio'],
      phone: json['phone'],
      city: json['city'],
      address: json['address'],
      office: json['office'],
      entrance: json['entrance'],
      doorPhone: json['doorphone'],
      floor: json['floor'],
      bonusSum: json['bonus_sum'],
      bonusPercent: json['bonus_percent'],
      orders: json['orders'],
    );
  }
}
