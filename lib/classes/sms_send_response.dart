import 'package:flutter/material.dart';

class SmsSendResponse {
  final bool success;
  final bool alreadySend;
  final String code;

  SmsSendResponse({
    @required this.alreadySend,
    @required this.success,
    @required this.code,
  });

  factory SmsSendResponse.fromJson(dynamic json) {
    if (json['datetime'] != null) {
      return SmsSendResponse(
        code: null,
        success: false,
        alreadySend: true,
      );
    }
    return SmsSendResponse(
      code: json['code'],
      success: json['result'] == 'Ok',
      alreadySend: false,
    );
  }

  factory SmsSendResponse.empty() {
    return SmsSendResponse(
      code: null,
      success: false,
      alreadySend: null,
    );
  }
}
