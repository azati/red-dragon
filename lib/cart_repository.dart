import 'package:red_dragon/classes/cart_product.dart';
import 'package:red_dragon/classes/order_history.dart';
import 'package:red_dragon/classes/product.dart';
import 'package:rxdart/rxdart.dart';

class CartRepository {
  static List<CartProduct> _productsInCart = [];
  static BehaviorSubject<List<CartProduct>> productInCartStream = BehaviorSubject();

  static void addProductToCart(Product product, int quantity) {
    bool isNewProduct = true;
    for (int i = 0; i < _productsInCart.length; i++) {
      if (_productsInCart[i].product.id == product.id) {
        _productsInCart[i].quantity += quantity;
        isNewProduct = false;
        break;
      }
    }
    if (isNewProduct) {
      _productsInCart.add(CartProduct(
        product: product,
        quantity: quantity,
      ));
    }
    productInCartStream.add(_productsInCart);
  }

  static void removeProductFromCart(Product product) {
    for (int i = 0; i < _productsInCart.length; i++) {
      if (_productsInCart[i].product.id == product.id) {
        _productsInCart.removeAt(i);
        break;
      }
    }
    productInCartStream.add(_productsInCart);
  }

  static void removeProductFromCartOnce(Product product) {
    for (int i = 0; i < _productsInCart.length; i++) {
      if (_productsInCart[i].product.id == product.id) {
        _productsInCart[i].quantity--;
        if (_productsInCart[i].quantity == 0) {
          _productsInCart.removeAt(i);
        }
        break;
      }
    }
    productInCartStream.add(_productsInCart);
  }

  static bool alreadyInCart(Product product) {
    for (int i = 0; i < _productsInCart.length; i++) {
      if (_productsInCart[i].product.id == product.id) {
        return true;
      }
    }
    return false;
  }

  static int countInCart(Product product) {
    for (int i = 0; i < _productsInCart.length; i++) {
      if (_productsInCart[i].product.id == product.id) {
        return _productsInCart[i].quantity;
      }
    }
    return 0;
  }

  static int differentProductsInCart() {
    return _productsInCart.length;
  }

  static int getCartProductPrice() {
    int sum = 0;
    for (int i = 0; i < _productsInCart.length; i++) {
      final price = _productsInCart[i].product.price;
      final quantity = _productsInCart[i].quantity;
      sum += (price * quantity);
    }
    return sum;
  }

  static bool isCartEmpty() {
    return _productsInCart.length == 0;
  }

  static List<CartProduct> getProducts() {
    List<CartProduct> products = [];
    for (int i = 0; i < _productsInCart.length; i++) {
      products.add(_productsInCart[i]);
    }
    return products;
  }

  static void fill(OrderHistory orderHistory) {
    List<CartProduct> products = [];
    for (int i = 0; i < orderHistory.products.length; i++) {
      final item = orderHistory.products[i];
      final cartProduct = CartProduct(
        product: Product(
          id: item.productId,
          name: item.name,
          price: item.price,
        ),
        quantity: item.quantity,
      );
      products.add(cartProduct);
    }
    _productsInCart = products;
    productInCartStream.add(_productsInCart);
  }

  static void clear() {
    _productsInCart.clear();
    productInCartStream.add(_productsInCart);
  }
}
