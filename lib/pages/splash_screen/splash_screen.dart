import 'package:flutter/material.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
        children: [
          Positioned.fill(
            child: Image(
              image: AssetImage('assets/background.png'),
              fit: BoxFit.fitWidth,
            ),
          ),
          Center(
            child: Image.asset(
              'assets/bigLogo.png',
              width: 200,
            ),
          )
        ],
      ),
    );
  }
}