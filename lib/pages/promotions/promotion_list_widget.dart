import 'package:flutter/material.dart';
import 'package:red_dragon/app_session_resources.dart';
import 'package:red_dragon/pages/auth/auth_repository.dart';
import 'package:red_dragon/pages/promotions/promotion_widget.dart';

class PromotionsMainPage extends StatefulWidget {
  @override
  _PromotionsMainPageState createState() => _PromotionsMainPageState();
}

class _PromotionsMainPageState extends State<PromotionsMainPage> {
  @override
  void initState() {
    super.initState();
    AuthRepository.authController.stream.listen((event) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(image: AssetImage('assets/background.png'), fit: BoxFit.cover),
      ),
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: AppSessionResources.promotions.length,
        itemBuilder: (context, index) {
          return PromotionWidget(
            promotion: AppSessionResources.promotions[index],
          );
        },
      ),
    );
  }
}
