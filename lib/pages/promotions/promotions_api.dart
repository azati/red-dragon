import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:red_dragon/classes/promotion.dart';

class PromotionsApi {
  static Future<List<Promotion>> getPromotions() async {
    final endpoint = 'https://rd-sushi.ru/api/stocks/index';
    final response = await http.get(endpoint);
    if (response.statusCode == 200) {
      List<dynamic> json = jsonDecode(response.body);
      List<Promotion> promotions = [];
      for (int i = 0; i < json.length; i++) {
        promotions.add(Promotion.fromJson(json[i]));
      }
      return promotions;
    } else {
      throw Exception('getPromotions response code is not 200');
    }
  }
}
