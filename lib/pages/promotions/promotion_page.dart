import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:red_dragon/classes/promotion.dart';
import 'package:red_dragon/widgets/app_scaffold.dart';

class PromotionPage extends StatelessWidget {
  final Promotion promotion;

  PromotionPage({@required this.promotion});

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      body: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(children: [
            Container(
              height: 250,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: CachedNetworkImageProvider(promotion.imageUrl),
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8),
              child: FloatingActionButton(
                mini: true,
                backgroundColor: Colors.white,
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Icon(
                  Icons.expand_more,
                  color: Colors.black,
                ),
              ),
            ),
          ]),
          Padding(
            padding: const EdgeInsets.fromLTRB(30, 20, 30, 20),
            child: Text(
              promotion.title,
              style: TextStyle(fontFamily: 'PTMono', fontSize: 18, letterSpacing: 0, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(30, 10, 30, 10),
            child: Text(
              promotion.description,
              style: TextStyle(
                fontFamily: 'PTSans',
                color: Colors.grey[600],
              ),
            ),
          ),
          _buildCouponeWidget()
        ],
      ),
    );
  }

  Widget _buildCouponeWidget() {
    if (promotion.code == null || promotion.code == "") {
      return Container();
    } else {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(30, 10, 30, 0),
            child: Row(
              children: [
                Text(
                  'Купон:',
                  style: TextStyle(
                    fontFamily: 'PTSans',
                    fontWeight: FontWeight.bold,
                    color: Colors.grey[600],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(30, 5, 30, 20),
            child: Row(
              children: [
                RaisedButton(
                  color: Colors.red[600],
                  child: Center(
                    child: Text(
                      promotion.code,
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'PTSans',
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  onPressed: () {
                    Clipboard.setData(
                      ClipboardData(
                        text: promotion.code,
                      ),
                    );
                    Fluttertoast.showToast(
                      msg: "Код скопирован",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIosWeb: 1,
                    );
                  },
                ),
              ],
            ),
          )
        ],
      );
    }
  }
}
