import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:red_dragon/classes/promotion.dart';
import 'package:red_dragon/pages/promotions/promotion_page.dart';

class PromotionWidget extends StatelessWidget {
  final Promotion promotion;

  PromotionWidget({@required this.promotion});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CachedNetworkImage(
              imageUrl: promotion.imageUrl,
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              promotion.title,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 16,
                fontFamily: 'PTMono',
              ),
            )
          ],
        ),
      ),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            fullscreenDialog: true,
            builder: (context) => PromotionPage(
              promotion: promotion,
            ),
          ),
        );
      },
    );
  }
}
