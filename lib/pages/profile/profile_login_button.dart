import 'package:flutter/material.dart';
import 'package:red_dragon/pages/auth/enterPhone/enter_phone_number_page.dart';

class LoginButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(30, 40, 30, 8),
      child: InkWell(
        onTap: () async {
          Navigator.push(
            context,
            MaterialPageRoute(
              fullscreenDialog: true,
              builder: (context) => EnterPhoneNumberPage(),
            ),
          );
        },
        child: Container(
          height: 50.0,
          decoration: BoxDecoration(
            color: Colors.red[600],
            borderRadius: BorderRadius.circular(25.0),
          ),
          child: Center(
            child: Text(
              'Авторизоваться',
              style: TextStyle(
                fontSize: 18.0,
                fontFamily: 'PTSans',
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
