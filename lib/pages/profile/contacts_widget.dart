import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:red_dragon/app_session_resources.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          'Служба доставки',
          style: TextStyle(
            fontFamily: 'PTMono',
            fontSize: 16,
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(40, 0, 40, 20),
          child: InkWell(
            child: Text(
              AppSessionResources.contacts.phone,
              style: TextStyle(
                fontFamily: 'PTSans',
                fontSize: 18,
              ),
            ),
            onTap: () {
              final formatterPhone = AppSessionResources.contacts.phone;
              final uri = "tel:$formatterPhone";
              launch(uri);
            },
          ),
        ),
        Text(
          'Адрес',
          style: TextStyle(
            fontFamily: 'PTMono',
            fontSize: 16,
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(40, 0, 40, 20),
          child: Text(
            AppSessionResources.contacts.address,
            style: TextStyle(
              fontFamily: 'PTSans',
              fontSize: 18,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        Text(
          'Режим работы',
          style: TextStyle(
            fontFamily: 'PTMono',
            fontSize: 16,
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(40, 0, 40, 20),
          child: Text(
            'Заказы принимаем с ${AppSessionResources.orderSettings.workFrom} до ${AppSessionResources.orderSettings.workTo} включительно',
            style: TextStyle(
              fontFamily: 'PTSans',
              fontSize: 18,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        Text(
          'Вопросы и предложения',
          style: TextStyle(
            fontFamily: 'PTMono',
            fontSize: 16,
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(40, 0, 40, 20),
          child: Text(
            AppSessionResources.contacts.chainEmail,
            style: TextStyle(
              fontFamily: 'PTSans',
              fontSize: 18,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(40, 0, 40, 0),
          child: IconButton(
            icon: ImageIcon(
              AssetImage('assets/inst.png'),
            ),
            onPressed: () {
              launch(AppSessionResources.contacts.instagramUrl);
            },
          ),
        ),
      ],
    );
  }
}
