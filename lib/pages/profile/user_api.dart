import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:red_dragon/app_session_resources.dart';
import 'package:red_dragon/classes/user.dart';
import 'package:red_dragon/pages/auth/auth_repository.dart';

class UserApi {
  static Future<User> getUser() async {
    final endpoint = 'https://rd-sushi.ru/api/check-access';
    final phone = await AuthRepository.getPhone();
    final token = await AuthRepository.getToken();

    final response = await http.post(endpoint, body: {
      'phone': phone,
      'access_token': token,
    });
    if (response.statusCode == 200) {
      dynamic json = jsonDecode(response.body);
      return User.fromJson(json);
    } else {
      throw Exception('getUser response code is not 200');
    }
  }

  static Future<User> updateUser({
    @required String phone,
    @required String name,
    @required String city,
    @required String address,
    @required String office,
    @required String entrance,
    @required String doorphone,
    @required String floor,
  }) async {
    final endpoint = 'https://rd-sushi.ru/api/users/change-data';
    final headers = {'Authorization': 'Bearer ${AppSessionResources.token}'};

    final body = {};

    if (phone != null) {
      body['phone'] = phone;
    }

    if (name != null) {
      body['fio'] = name;
    }

    if (city != null) {
      body['city'] = city;
    }

    if (address != null) {
      body['address'] = address;
    }

    if (office != null) {
      body['office'] = office;
    }

    if (entrance != null) {
      body['entrance'] = entrance;
    }

    if (doorphone != null) {
      body['doorphone'] = doorphone;
    }

    if (floor != null) {
      body['floor'] = floor;
    }

    final response = await http.post(
      endpoint,
      body: body,
      headers: headers,
    );

    if (response.statusCode == 200) {
      dynamic json = jsonDecode(response.body);
      return User.fromJson(json);
    } else {
      throw Exception('updateUser response code is not 200');
    }
  }
}
