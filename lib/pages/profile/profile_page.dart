import 'package:flutter/material.dart';
import 'package:red_dragon/app_session_resources.dart';
import 'package:red_dragon/pages/auth/auth_repository.dart';
import 'package:red_dragon/pages/profile/contacts_widget.dart';
import 'package:red_dragon/pages/profile/profile_login_button.dart';
import 'package:red_dragon/pages/profile/profile_authorized_widget.dart';

class ProfileMainPage extends StatefulWidget {
  @override
  _ProfileMainPageState createState() => _ProfileMainPageState();
}

class _ProfileMainPageState extends State<ProfileMainPage> {
  @override
  void initState() {
    super.initState();
    AuthRepository.authController.stream.listen((event) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(image: AssetImage('assets/background.png'), fit: BoxFit.cover),
      ),
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            AppSessionResources.token != null
                ? AuthorizedProfileWidget()
                : Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 20,
                    ),
                    child: LoginButton(),
                  ),
            ContactsWidget(),
          ],
        ),
      ),
    );
  }
}
