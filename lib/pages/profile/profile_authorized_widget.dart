import 'package:flutter/material.dart';
import 'package:red_dragon/app_session_resources.dart';
import 'package:red_dragon/pages/orderHistoryList/orders_history_page.dart';
import 'package:red_dragon/pages/profile_settings/profile_settings_page.dart';

class AuthorizedProfileWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: InkWell(
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: new BorderRadius.circular(15.0),
                      boxShadow: [
                        new BoxShadow(
                          color: Colors.grey[200],
                          blurRadius: 1,
                          spreadRadius: 2,
                        )
                      ],
                    ),
                    height: 80,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                AppSessionResources.user.fio,
                                style: TextStyle(
                                  fontFamily: 'PTSans',
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Icon(
                                Icons.edit,
                                color: Colors.grey[600],
                                size: 20,
                              ),
                            )
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
                              child: Text(
                                AppSessionResources.user.phone,
                                style: TextStyle(
                                  fontFamily: 'PTSans',
                                  fontSize: 16,
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ProfileSettingsPage(),
                      ),
                    );
                  },
                ),
              ),
              Container(
                width: 16,
              ),
              Expanded(
                child: InkWell(
                  child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: new BorderRadius.circular(15.0),
                        boxShadow: [
                          new BoxShadow(
                            color: Colors.grey[200],
                            blurRadius: 1,
                            spreadRadius: 2,
                          ),
                        ],
                      ),
                      height: 80,
                      //color: Colors.red,
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Container(
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.red[50],
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    AppSessionResources.user.bonusSum.toString(),
                                    style: TextStyle(
                                      fontFamily: 'PTSans',
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18,
                                      color: Colors.red[600],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
                              child: Text(
                                'Мои бонусы',
                                style: TextStyle(
                                  fontFamily: 'PTSans',
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                ),
                              ),
                            )
                          ],
                        ),
                      )),
                  onTap: () {},
                ),
              )
            ],
          ),
        ),
        Row(
          children: [
            Expanded(
              child: Padding(
                padding: EdgeInsets.fromLTRB(16, 0, 16, 16),
                child: InkWell(
                  child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: new BorderRadius.circular(25.0),
                        boxShadow: [new BoxShadow(color: Colors.grey[200], blurRadius: 1, spreadRadius: 2)],
                      ),
                      height: 50,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Мои заказы',
                              style: TextStyle(fontFamily: 'PTSans', fontWeight: FontWeight.bold, fontSize: 18),
                            ),
                            Image(
                              image: AssetImage('assets/next.png'),
                            )
                          ],
                        ),
                      )),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => OrdersHistoryPage(),
                      ),
                    );
                  },
                ),
              ),
            )
          ],
        )
      ],
    );
  }
}
