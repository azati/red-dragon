import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:red_dragon/pages/cart/buy_products_button.dart';

class OrderAcceptedPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 20,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'Спасибо за заказ',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'PTMono',
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Container(
                  height: 24,
                ),
                Text(
                  'В ближайшее время наш менеджер свяжется с вами для подтверждения заказа',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'PTSans',
                    fontSize: 16,
                  ),
                ),
                Container(
                  height: 24,
                ),
                Image.asset(
                  'assets/bike.png',
                  width: 200,
                ),
                Container(
                  height: 24,
                ),
                AppButton(
                  title: 'Хорошо, спасибо!',
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
