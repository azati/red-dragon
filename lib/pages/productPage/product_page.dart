import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:red_dragon/cart_repository.dart';
import 'package:red_dragon/classes/product.dart';
import 'package:red_dragon/pages/productPage/change_product_count_widget.dart';
import 'package:red_dragon/widgets/app_scaffold.dart';
import 'package:red_dragon/widgets/wide_add_to_cart_button.dart';

class ProductPage extends StatefulWidget {
  final Product product;
  final Function updateListener;

  ProductPage({@required this.product, this.updateListener});

  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  int count = 1;

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Stack(
            children: [
              Container(
                height: 250,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: CachedNetworkImageProvider(widget.product.imageUrl),
                    fit: BoxFit.fitHeight,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8),
                child: FloatingActionButton(
                  mini: true,
                  backgroundColor: Colors.white,
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Icon(
                    Icons.expand_more,
                    color: Colors.black,
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 8),
            child: Text(
              widget.product.name,
              style: TextStyle(
                fontFamily: 'PTMono',
                fontWeight: FontWeight.bold,
                fontSize: 24,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 8),
            child: Text(
              widget.product.description,
              style: TextStyle(
                fontFamily: 'PTSans',
                fontSize: 16,
                color: Colors.grey[700],
              ),
            ),
          ),
          _buildAdditionalInfoWidget(),
        ],
      ),
      pinnedBottomWidget: Padding(
        padding: const EdgeInsets.only(bottom: 16, left: 16, right: 16),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    '${widget.product.price * count} ₽',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 26,
                      fontFamily: 'PTSans',
                    ),
                  ),
                ),
                ChangeProductCountWidget(
                  changeListener: (count) {
                    setState(() {
                      this.count = count;
                    });
                  },
                )
              ],
            ),
            SizedBox(
              height: 16,
            ),
            WideAddToCartButton(
              onTap: () {
                CartRepository.addProductToCart(widget.product, count);
                widget.updateListener.call();
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildAdditionalInfoWidget() {
    List<Widget> widgets = [];
    if (widget.product.weight != null) {
      widgets.add(
        Expanded(
          child: _buildMoreInfoItem('Вес', "${widget.product.weight} г"),
        ),
      );
      widgets.add(
        SizedBox(
          width: 16,
        ),
      );
    }
    if (widget.product.quantity != null) {
      widgets.add(
        Expanded(
          child: _buildMoreInfoItem('Колличество', "${widget.product.quantity} шт"),
        ),
      );
      widgets.add(
        SizedBox(
          width: 16,
        ),
      );
    }
    if (widget.product.calorie != null) {
      widgets.add(
        Expanded(
          child: _buildMoreInfoItem('Калорийность', "${widget.product.calorie} ккал"),
        ),
      );
      widgets.add(
        SizedBox(
          width: 16,
        ),
      );
    }
    if (widgets.length > 0) {
      widgets.removeLast();
    }
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 8),
      child: Row(
        children: widgets,
      ),
    );
  }

  Widget _buildMoreInfoItem(String title, String description) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Colors.grey[200],
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              description,
              style: TextStyle(
                fontFamily: 'PTSans',
                fontSize: 15,
                fontWeight: FontWeight.bold,
              ),
              overflow: TextOverflow.ellipsis,
            ),
            Text(
              title,
              style: TextStyle(
                fontFamily: 'PTSans',
                fontSize: 11,
                color: Colors.grey[700],
              ),
              overflow: TextOverflow.ellipsis,
            )
          ],
        ),
      ),
    );
  }
}