import 'package:flutter/material.dart';

class ChangeProductCountWidget extends StatefulWidget {
  final Function(int) changeListener;

  const ChangeProductCountWidget({Key key, this.changeListener}) : super(key: key);

  @override
  _ChangeProductCountWidgetState createState() => _ChangeProductCountWidgetState();
}

class _ChangeProductCountWidgetState extends State<ChangeProductCountWidget> {
  int count = 1;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          RawMaterialButton(
            onPressed: () {
              if (count > 1) {
                count--;
                widget.changeListener.call(count);
                setState(() {});
              }
            },
            constraints: BoxConstraints(),
            elevation: 2.0,
            fillColor: Colors.white,
            child: Icon(
              Icons.remove,
              size: 20.0,
              color: Colors.black,
            ),
            padding: EdgeInsets.all(8.0),
            shape: CircleBorder(),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              count.toString(),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                fontFamily: 'PTSans',
              ),
            ),
          ),
          RawMaterialButton(
            onPressed: () {
              count++;
              widget.changeListener.call(count);
              setState(() {

              });
            },
            constraints: BoxConstraints(),
            elevation: 2.0,
            fillColor: Colors.red[600],
            child: Icon(
              Icons.add,
              size: 20.0,
              color: Colors.white,
            ),
            padding: EdgeInsets.all(8.0),
            shape: CircleBorder(),
          )
        ],
      ),
    );
  }
}