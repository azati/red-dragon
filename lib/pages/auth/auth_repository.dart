import 'dart:async';
import 'package:meta/meta.dart';
import 'package:red_dragon/app_session_resources.dart';
import 'package:red_dragon/classes/user.dart';
import 'package:red_dragon/pages/auth/auth_api.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum AuthStatus { authenticated, unauthenticated }

class AuthRepository {

  static const String _USER_TOKEN_KEY = "user_token";
  static const String _USER_PHONE_KEY = "user_phone";

  static StreamController authController = BehaviorSubject<AuthStatus>();

  static Future<bool> isAuth() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userToken = prefs.get(_USER_TOKEN_KEY);
    if (userToken != null && userToken != "") {
      return true;
    } else {
      return false;
    }
  }

  static Future<void> _saveToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(_USER_TOKEN_KEY, token);
  }

  static Future<void> _savePhone(String phone) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(_USER_PHONE_KEY, phone);
  }

  static Future<String> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(_USER_TOKEN_KEY);
  }

  static Future<String> getPhone() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(_USER_PHONE_KEY);
  }

  static Future<void> _clearToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  static Future<bool> logIn({
    @required String phone,
    @required String code,
  }) async {
    final authResponse = await AuthApi.logIn(phone: phone, code: code);
    if (authResponse.success) {
      await _saveToken(authResponse.user.accessToken);
      await _savePhone(phone);
      AppSessionResources.token = authResponse.user.accessToken;
      AppSessionResources.user = authResponse.user;
      await AppSessionResources.init();
      authController.add(AuthStatus.authenticated);
      return true;
    } else {
      return false;
    }
  }

  static Future<void> logOut() async {
    await _clearToken();
    AppSessionResources.clear();
    authController.add(AuthStatus.unauthenticated);
  }
}
