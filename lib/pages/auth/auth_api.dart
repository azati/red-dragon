import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:red_dragon/classes/auth_response.dart';
import 'package:red_dragon/classes/sms_send_response.dart';

class AuthApi {
  static Future<AuthResponse> logIn({
    @required String phone,
    @required String code,
  }) async {
    final response = await http.post(
      'https://rd-sushi.ru/api/login',
      body: {
        'phone': phone,
        'code': code,
      },
    );
    if (response.statusCode == 201) {
      final json = jsonDecode(response.body);
      return AuthResponse.fromJson(json);
    } else {
      return AuthResponse.empty();
    }
  }

  static Future<SmsSendResponse>  sendCode({
    @required String phone,
  }) async {
    final response = await http.post(
      'https://rd-sushi.ru/api/send-code',
      body: {
        'phone': phone,
      },
    );
    final json = jsonDecode(response.body);
    return SmsSendResponse.fromJson(json);
  }
}
