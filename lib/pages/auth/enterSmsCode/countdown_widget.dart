import 'package:flutter/material.dart';

class CountdownWidget extends AnimatedWidget {
  CountdownWidget({Key key, this.animation}) : super(key: key, listenable: animation);
  final Animation<int> animation;

  @override
  build(BuildContext context) {
    return Text(
      getTime(animation.value),
      style: TextStyle(
        fontFamily: 'PTSans',
        fontSize: 14,
      ),
    );
  }

  String getTime(int timeValue) {
    int minutes = (timeValue ~/ 60).toInt();
    int seconds = timeValue % 60;
    String times = minutes.toString() + ':';
    if (seconds < 10) {
      times = times + '0' + seconds.toString();
    } else {
      times = times + seconds.toString();
    }
    return times;
  }
}
