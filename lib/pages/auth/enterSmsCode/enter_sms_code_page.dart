import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:red_dragon/pages/auth/auth_api.dart';
import 'package:red_dragon/pages/auth/auth_repository.dart';
import 'package:red_dragon/pages/auth/enterSmsCode/countdown_widget.dart';
import 'package:red_dragon/pages/auth/enterSmsCode/sms_code_field.dart';
import 'package:red_dragon/pages/cart/buy_products_button.dart';
import 'package:red_dragon/widgets/helper.dart';

class EnterSmsCodePage extends StatefulWidget {
  final String phone;

  EnterSmsCodePage({@required this.phone});

  @override
  _EnterSmsCodePageState createState() => _EnterSmsCodePageState();
}

class _EnterSmsCodePageState extends State<EnterSmsCodePage> with TickerProviderStateMixin {
  final TextEditingController controller = TextEditingController();

  final int kStartValue = 2 * 60;
  bool sendSmsButtonVisible = false;
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = new AnimationController(
      vsync: this,
      duration: new Duration(seconds: kStartValue),
    );
    _controller.addListener(() {
      if (_controller.isCompleted) {
        setState(() {
          sendSmsButtonVisible = true;
        });
      }
    });
    _controller.forward(from: 0.0);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned.fill(
            child: Image(
              image: AssetImage('assets/background.png'),
              fit: BoxFit.fitWidth,
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 20, 0, 0),
            child: IconButton(
              icon: Icon(Icons.keyboard_arrow_left),
              color: Colors.black,
              onPressed: () async {
                Navigator.pop(context);
              },
            ),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Код из СМС',
                  style: TextStyle(
                    fontFamily: 'PTMono',
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(60, 10, 60, 20),
                  child: Text(
                    'Введите код подтверждения, отпраленнынй на номер  ',
                    style: TextStyle(fontFamily: 'PTSans', fontSize: 14),
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(64, 0, 64, 0),
                  child: Container(
                    width: 128,
                    child: SmsCodeField(
                      controller: controller,
                    ),
                  ),
                ),
                sendSmsButtonVisible
                    ? Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: AppButton(
                          title: 'Отправить повторно',
                          onTap: () async {
                            sendSmsButtonVisible = false;
                            _controller.forward(from: 0.0);
                            setState(() {});
                            final response = await AuthApi.sendCode(
                              phone: widget.phone,
                            );
                            if (response.success) {
                              print(response.code);
                              Fluttertoast.showToast(
                                msg: "Код отправлен",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIosWeb: 1,
                              );
                            } else {
                              showAlert(context, 'Ошибка', 'Не удалось отправить код.');
                            }
                          },
                        ),
                    )
                    : Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Отправить повторно через  ',
                            style: TextStyle(fontFamily: 'PTSans', fontSize: 14),
                            textAlign: TextAlign.center,
                          ),
                          CountdownWidget(
                            animation: StepTween(
                              begin: kStartValue,
                              end: 0,
                            ).animate(_controller),
                          ),
                        ],
                      ),
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: AppButton(
                    title: 'Готово',
                    onTap: () async {
                      RegExp exp = RegExp(r"[^+0-9]+");
                      String code = controller.text.replaceAll(exp, '');
                      print(code);
                      if (code.length != 4) {
                        showAlert(context, 'Ошибка', 'Введен неверный код.');
                        return;
                      }
                      showLoadingAlert(context);
                      final isAuthenticated = await AuthRepository.logIn(
                        phone: widget.phone,
                        code: code,
                      );
                      Navigator.of(context).pop();
                      if (isAuthenticated) {
                        Navigator.of(context).pop();
                        Navigator.of(context).pop();
                      } else {
                        showAlert(context, 'Ошибка', 'Введен неверный код.');
                      }
                    },
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
