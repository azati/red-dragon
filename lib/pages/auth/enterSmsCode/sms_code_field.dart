import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class SmsCodeField extends StatelessWidget {
  final TextEditingController controller;
  final maskTextInputFormatter = MaskTextInputFormatter(
    mask: "# # # #",
    filter: {
      "#": RegExp(r'[0-9]'),
    },
  );

  SmsCodeField({
    @required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: TextField(
        style: TextStyle(fontSize: 20, fontFamily: 'PTSans'),
        controller: controller,
        inputFormatters: [maskTextInputFormatter],
        autocorrect: false,
        keyboardType: TextInputType.phone,
        cursorColor: Colors.red[600],
        decoration: InputDecoration(
          hintText: "– – – –",
          fillColor: Colors.white,
          filled: true,
          border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
            borderRadius: BorderRadius.circular(25.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.red[600]),
            borderRadius: BorderRadius.circular(25.0),
          ),
        ),
      ),
    );
  }
}
