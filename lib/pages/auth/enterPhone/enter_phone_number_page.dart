import 'package:flutter/material.dart';
import 'package:red_dragon/pages/auth/auth_api.dart';
import 'package:red_dragon/pages/auth/enterPhone/phone_number_field.dart';
import 'package:red_dragon/pages/auth/enterSmsCode/enter_sms_code_page.dart';
import 'package:red_dragon/pages/cart/buy_products_button.dart';
import 'package:red_dragon/widgets/helper.dart';

class EnterPhoneNumberPage extends StatelessWidget {
  final phoneController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned.fill(
            child: Image(
              image: AssetImage('assets/background.png'),
              fit: BoxFit.fitWidth,
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 20, 0, 0),
            child: IconButton(
              icon: Icon(Icons.close),
              color: Colors.black,
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 16,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Укажите номер телефона',
                    style: TextStyle(
                      fontFamily: 'PTMono',
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8),
                    child: Text(
                      'Мы вышлем код подтверждения на ваш номер телефона',
                      style: TextStyle(fontFamily: 'PTSans', fontSize: 14),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16),
                    child: PhoneNumberField(
                      controller: phoneController,
                    ),
                  ),
                  AppButton(
                    title: 'Авторизоваться',
                    onTap: () async {
                      RegExp exp = RegExp(r"[^+0-9]+");
                      String phone = phoneController.text.replaceAll(exp, '');
                      print(phone);
                      if (phone.length != 12) {
                        showAlert(context, 'Ошибка', 'Некорректный номер телефона.');
                        return;
                      }
                      showLoadingAlert(context);
                      final response = await AuthApi.sendCode(
                        phone: phone,
                      );
                      Navigator.of(context).pop();
                      if (response.success) {
                        print(response.code);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => EnterSmsCodePage(
                              phone: phone,
                            ),
                          ),
                        );
                      } else if (response.alreadySend) {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => EnterSmsCodePage(
                              phone: phone,
                            ),
                          ),
                        );
                      }
                    },
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
