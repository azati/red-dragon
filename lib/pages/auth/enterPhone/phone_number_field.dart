import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class PhoneNumberField extends StatelessWidget {
  final TextEditingController controller;

  PhoneNumberField({
    this.controller,
  });

  final maskTextInputFormatter = MaskTextInputFormatter(
    mask: "+7 (###) ###-##-##",
    filter: {
      "#": RegExp(r'[0-9]'),
    },
  );

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      inputFormatters: [maskTextInputFormatter],
      autocorrect: false,
      keyboardType: TextInputType.phone,
      cursorColor: Colors.red[600],
      decoration: InputDecoration(
        hintText: "+7 (800) 555-35-35",
        fillColor: Colors.white,
        filled: true,
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
          borderRadius: BorderRadius.circular(25.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.red[600]),
          borderRadius: BorderRadius.circular(25.0),
        ),
      ),
    );
  }
}
