import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:red_dragon/app_session_resources.dart';
import 'package:red_dragon/cart_repository.dart';
import 'package:red_dragon/classes/cart_product.dart';
import 'package:red_dragon/pages/auth/auth_repository.dart';
import 'package:red_dragon/pages/auth/enterPhone/enter_phone_number_page.dart';
import 'package:red_dragon/pages/cart/recommendation_list_widget.dart';
import 'package:red_dragon/pages/confirmOrder/confirm_order_page.dart';
import 'package:red_dragon/pages/main/main_navigator.dart';

import 'buy_products_button.dart';
import 'cart_item.dart';

class NonEmptyCartWidget extends StatefulWidget {
  final Function onCartEmptyListener;

  const NonEmptyCartWidget({Key key, this.onCartEmptyListener}) : super(key: key);

  @override
  _NonEmptyCartWidgetState createState() => _NonEmptyCartWidgetState();
}

class _NonEmptyCartWidgetState extends State<NonEmptyCartWidget> {
  List<CartProduct> cartProducts = [];

  @override
  void initState() {
    super.initState();
    CartRepository.productInCartStream.listen((products) {
      setState(() {
        this.cartProducts = products;
        if (products.length == 0) {
          widget.onCartEmptyListener.call();
        }
      });
    });
    AuthRepository.authController.stream.listen((event) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(image: AssetImage('assets/background.png'), fit: BoxFit.cover),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
            child: ListView.builder(
              shrinkWrap: true,
              primary: false,
              itemCount: cartProducts.length + 1,
              itemBuilder: (context, index) {
                print(index);
                if (index == cartProducts.length) {
                  return RecommendationListWidget();
                }
                return CartItem(
                  cartProduct: cartProducts[index],
                  onRemoveFromCart: () {
                    CartRepository.removeProductFromCart(cartProducts[index].product);
                  },
                  productAmountChanged: () {
                    setState(() {});
                  },
                );
              },
            ),
          ),
          _buildBottomWidget()
        ],
      ),
    );
  }

  Widget _buildBottomWidget() {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(16, 8, 16, 16),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Итого:',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontFamily: 'PTSans',
                    fontSize: 16,
                  ),
                ),
                Text(
                  '${CartRepository.getCartProductPrice()} ₽',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontFamily: 'PTSans',
                    fontSize: 20,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 8,
            ),
            _buildBuyWidget()
          ],
        ),
      ),
    );
  }

  Widget _buildBuyWidget() {
    if (AppSessionResources.orderSettings != null) {
      if (CartRepository.getCartProductPrice() > AppSessionResources.orderSettings.minSum) {
        return AppButton(
          title: 'Оформить Заказ',
          onTap: () {
            if (AppSessionResources.token != null) {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => ConfirmOrderPage(),
                ),
              );
            } else {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => EnterPhoneNumberPage(),
                ),
              );
            }
          },
        );
      } else {
        return Center(
          child: Text(
            'Минимальная сумма заказа для оформления доставки ${AppSessionResources.orderSettings.minSum} р',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontFamily: 'PTMono',
              color: Colors.red,
              fontSize: 12,
            ),
          ),
        );
      }
    }
    return Container();
  }
}
