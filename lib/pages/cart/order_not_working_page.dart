import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:red_dragon/app_session_resources.dart';

class OrderNotWorking extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(image: AssetImage('assets/background.png'), fit: BoxFit.cover),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text(
                'Внимание! Сейчас доставка не доступна',
                style: TextStyle(
                  fontSize: 18,
                  fontFamily: 'PTMono',
                  fontWeight: FontWeight.bold,
                  color: Colors.red,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text(
                'Режим работы доставки с ${AppSessionResources.orderSettings.workFrom} до ${AppSessionResources.orderSettings.workTo}',
                style: TextStyle(
                  fontSize: 18,
                  fontFamily: 'PTSans',
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
