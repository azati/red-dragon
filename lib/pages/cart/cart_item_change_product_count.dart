import 'package:flutter/material.dart';

class CartItemChangeProductCount extends StatefulWidget {
  final Function(int) changeListener;
  final int initialQuantity;

  const CartItemChangeProductCount({
    Key key,
    this.changeListener,
    this.initialQuantity,
  }) : super(key: key);

  @override
  _CartItemChangeProductCountState createState() => _CartItemChangeProductCountState();
}

class _CartItemChangeProductCountState extends State<CartItemChangeProductCount> {
  int count;

  @override
  void initState() {
    super.initState();
    count = widget.initialQuantity;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          RawMaterialButton(
            onPressed: () {
              count--;
              widget.changeListener.call(-1);
              setState(() {});
            },
            constraints: BoxConstraints(),
            elevation: 2.0,
            fillColor: Colors.white,
            child: Icon(
              Icons.remove,
              size: 15.0,
              color: Colors.black,
            ),
            padding: EdgeInsets.all(8.0),
            shape: CircleBorder(),
          ),
          Text(
            count.toString(),
            style: TextStyle(fontFamily: 'PTSans', fontSize: 18, fontWeight: FontWeight.bold),
          ),
          RawMaterialButton(
            onPressed: () {
              count++;
              widget.changeListener.call(1);
              setState(() {});
            },
            constraints: BoxConstraints(),
            elevation: 2.0,
            fillColor: Colors.red[600],
            child: Icon(
              Icons.add,
              size: 15.0,
              color: Colors.white,
            ),
            padding: EdgeInsets.all(8.0),
            shape: CircleBorder(),
          )
        ],
      ),
    );
  }
}
