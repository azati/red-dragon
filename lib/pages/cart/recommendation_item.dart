import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:red_dragon/app_session_resources.dart';
import 'package:red_dragon/cart_repository.dart';
import 'package:red_dragon/classes/product.dart';

class RecommendationItem extends StatelessWidget {
  final Product product;

  const RecommendationItem({
    Key key,
    this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: 200,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
          boxShadow: [
            BoxShadow(
              color: Colors.grey[200],
              blurRadius: 1,
              spreadRadius: 2,
            ),
          ],
        ),
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 8),
              child: CachedNetworkImage(
                imageUrl: product.imageUrl,
                height: 50,
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      product.name,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontFamily: 'PTMono',
                        fontSize: 16,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '${product.price} ₽',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontFamily: 'PTMono',
                            fontSize: 16,
                          ),
                        ),
                        _AddButton(
                          product: product,
                        ),
                      ],
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}


class _AddButton extends StatelessWidget {
  final Product product;

  const _AddButton({Key key, this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: () {
        CartRepository.addProductToCart(product, 1);
      },
      constraints: BoxConstraints(),
      elevation: 2.0,
      fillColor: Colors.red[600],
      child: Icon(
        Icons.add,
        size: 15.0,
        color: Colors.white,
      ),
      padding: EdgeInsets.all(6.0),
      shape: CircleBorder(),
    );
  }
}
