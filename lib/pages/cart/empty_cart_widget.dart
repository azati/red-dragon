import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:red_dragon/pages/main/main_navigator.dart';

import 'buy_products_button.dart';

class EmptyCartWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(image: AssetImage('assets/background.png'), fit: BoxFit.cover),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text(
                'Ваша корзина пуста',
                style: TextStyle(
                  fontSize: 18,
                  fontFamily: 'PTMono',
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text(
                'Ваша корзина пуста, откройте “Меню” и выберите товары',
                style: TextStyle(
                  fontSize: 16,
                  fontFamily: 'PTSans',
                  fontWeight: FontWeight.w300,
                  color: Colors.black,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: AppButton(
                title: 'Перейти в меню',
                onTap: () {
                  MainNavigator.navigateToIndex(0);
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
