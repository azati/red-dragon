import 'package:flutter/material.dart';
import 'package:red_dragon/app_session_resources.dart';
import 'package:red_dragon/cart_repository.dart';
import 'package:red_dragon/pages/cart/order_not_working_page.dart';

import 'empty_cart_widget.dart';
import 'non_empty_cart_widget.dart';

class CartMainPage extends StatefulWidget {
  @override
  _CartMainPageState createState() => _CartMainPageState();
}

class _CartMainPageState extends State<CartMainPage> {
  @override
  Widget build(BuildContext context) {
    return _buildContent();
  }

  Widget _buildContent() {
    if (!AppSessionResources.isOrderWorkingNow()) {
      return OrderNotWorking();
    } else if (CartRepository.isCartEmpty()) {
      return EmptyCartWidget();
    } else {
      return NonEmptyCartWidget(
        onCartEmptyListener: () {
          setState(() {},);
        },
      );
    }
  }
}
