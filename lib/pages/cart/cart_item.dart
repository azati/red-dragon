import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:red_dragon/classes/cart_product.dart';
import 'package:red_dragon/pages/cart/cart_item_change_product_count.dart';

class CartItem extends StatefulWidget {
  final CartProduct cartProduct;
  final Function() onRemoveFromCart;
  final Function() productAmountChanged;

  CartItem({
    @required this.cartProduct,
    this.onRemoveFromCart,
    this.productAmountChanged,
  });

  @override
  _CartItemState createState() => _CartItemState();
}

class _CartItemState extends State<CartItem> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [new BoxShadow(color: Colors.grey[200], blurRadius: 1, spreadRadius: 2)],
        ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
          child: Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Container(
                    height: 110,
                    width: 140,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: CachedNetworkImageProvider(widget.cartProduct.product.imageUrl),
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.circular(20),
                    ),
                  ),
                ),
                flex: 2,
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              widget.cartProduct.product.name,
                              style: TextStyle(fontWeight: FontWeight.bold, fontFamily: 'PTMono', fontSize: 18),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          IconButton(
                            icon: Icon(Icons.clear),
                            onPressed: () {
                              widget.onRemoveFromCart.call();
                            },
                          )
                        ],
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 48, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            if (widget.cartProduct.product.weight != null)
                              Text(
                                '${widget.cartProduct.product.weight} г',
                                style: TextStyle(fontSize: 12, fontFamily: 'PTSans', color: Colors.grey[700]),
                              ),
                            if (widget.cartProduct.product.quantity != null)
                              Text(
                                '${widget.cartProduct.product.quantity} шт',
                                style: TextStyle(fontSize: 12, fontFamily: 'PTSans', color: Colors.grey[700]),
                              ),
                            if (widget.cartProduct.product.calorie != null)
                              Text(
                                '${widget.cartProduct.product.calorie} ккал',
                                style: TextStyle(fontSize: 12, fontFamily: 'PTSans', color: Colors.grey[700]),
                              ),
                          ],
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            '${widget.cartProduct.product.price} ₽',
                            style: TextStyle(
                              fontFamily: 'PTSans',
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                          ),
                          CartItemChangeProductCount(
                            changeListener: (add) {
                              widget.cartProduct.quantity += add;
                              setState(() {});
                              if (widget.cartProduct.quantity == 0) {
                                widget.onRemoveFromCart.call();
                              }
                              widget.productAmountChanged.call();
                            },
                            initialQuantity: widget.cartProduct.quantity,
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                flex: 3,
              )
            ],
          ),
        ),
      ),
    );
  }
}
