import 'package:flutter/material.dart';
import 'package:red_dragon/app_session_resources.dart';
import 'package:red_dragon/pages/cart/recommendation_item.dart';

class RecommendationListWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    if (AppSessionResources.additionalProducts.length == 0) {
      return Container();
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(16, 8, 0, 8),
          child: Text(
            'Рекомендуем',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontFamily: 'PTMono',
              fontSize: 18,
            ),
          ),
        ),
        Container(
          height: 90,
          child: ListView.builder(
            padding: EdgeInsets.symmetric(horizontal: 8),
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              return RecommendationItem(
                product: AppSessionResources.additionalProducts[index],
              );
            },
            itemCount: AppSessionResources.additionalProducts.length,
          ),
        )
      ],
    );
  }
}
