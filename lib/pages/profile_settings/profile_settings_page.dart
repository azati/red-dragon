import 'package:flutter/material.dart';
import 'package:red_dragon/app_session_resources.dart';
import 'package:red_dragon/classes/OrderCity.dart';
import 'package:red_dragon/pages/auth/auth_repository.dart';
import 'package:red_dragon/pages/profile/user_api.dart';
import 'package:red_dragon/pages/profile_settings/profile_settings_phone_field.dart';
import 'package:red_dragon/pages/profile_settings/save_profile_settings_page.dart';
import 'package:red_dragon/widgets/app_input.dart';
import 'package:red_dragon/widgets/app_scaffold.dart';
import 'package:red_dragon/widgets/helper.dart';

class ProfileSettingsPage extends StatefulWidget {
  @override
  _ProfileSettingsPageState createState() => _ProfileSettingsPageState();
}

class _ProfileSettingsPageState extends State<ProfileSettingsPage> {
  final TextEditingController nameController = TextEditingController();

  final TextEditingController phoneController = TextEditingController();

  final TextEditingController addressController = TextEditingController();

  final TextEditingController flatController = TextEditingController();
  final TextEditingController entranceController = TextEditingController();
  final TextEditingController codeController = TextEditingController();
  final TextEditingController floorController = TextEditingController();

  String selectedCity;

  @override
  void initState() {
    super.initState();
    nameController.text = AppSessionResources.user.fio;
    phoneController.text = AppSessionResources.user.phone;
    addressController.text = AppSessionResources.user.address;
    flatController.text = AppSessionResources.user.office;
    entranceController.text = AppSessionResources.user.entrance;
    codeController.text = AppSessionResources.user.doorPhone;
    floorController.text = AppSessionResources.user.floor;
    if (AppSessionResources.user.city.isNotEmpty) {
      selectedCity = AppSessionResources.user.city;
    }
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      appBarTitle: 'Настройки',
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 16,
              vertical: 16,
            ),
            child: Text(
              'Телефон',
              style: TextStyle(
                fontFamily: 'PTMono',
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: ProfileSettingsPhoneField(
              controller: phoneController,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            child: Text(
              'Имя',
              style: TextStyle(
                fontFamily: 'PTMono',
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Container(
              height: 50,
              child: TextField(
                controller: nameController,
                cursorColor: Colors.red[600],
                decoration: InputDecoration(
                  hintText: "Имя",
                  fillColor: Colors.white,
                  filled: true,
                  border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey[300]),
                    borderRadius: BorderRadius.circular(25.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red[600]),
                    borderRadius: BorderRadius.circular(25.0),
                  ),
                ),
              ),
            ),
          ),
          _buildAddressWidget(),
          SizedBox(
            height: 16,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: SaveProfileSettingsButton(
              onTap: () async {
                showLoadingAlert(context);
                UserApi.updateUser(
                  phone: phoneController.text,
                  name: nameController.text,
                  city: selectedCity,
                  address: addressController.text,
                  office: flatController.text,
                  entrance: entranceController.text,
                  doorphone: codeController.text,
                  floor: floorController.text,
                ).then(
                  (user) {
                    AppSessionResources.user = user;
                    Navigator.of(context).pop();
                    showAlert(context, 'Успех', 'Информация обновлена');
                  },
                ).catchError((error) {
                  Navigator.of(context).pop();
                  showAlert(context, 'Ошибка', 'Не удалось обновить данные пользователя. Попробуйте позже.');
                });
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Center(
              child: FlatButton(
                color: Colors.transparent,
                child: Text(
                  'Выйти',
                  style: TextStyle(
                    fontFamily: 'PTSans',
                    fontSize: 18,
                  ),
                ),
                onPressed: () async {
                  await AuthRepository.logOut();
                  Navigator.of(context).pop();
                },
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildAddressWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 16, 0, 16),
                child: Text(
                  'Адрес доставки',
                  style: TextStyle(
                    fontFamily: 'PTMono',
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Container(
            padding: const EdgeInsets.only(top: 4.0, left: 16.0, right: 16.0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(30.0),
              border: Border.all(color: Colors.grey),
            ),
            child: Center(
              child: DropdownButtonHideUnderline(
                child: DropdownButton(
                  isExpanded: true,
                  style: TextStyle(
                    fontFamily: 'PTMono',
                    fontSize: 18.0,
                  ),
                  value: selectedCity,
                  hint: Text('Город'),
                  items: AppSessionResources.cities.map((OrderCity city) {
                    return DropdownMenuItem(
                      value: city.name,
                      child: Text(
                        city.name,
                        style: TextStyle(
                          fontFamily: 'PTMono',
                          color: Colors.black,
                        ),
                      ),
                    );
                  }).toList(),
                  onChanged: (String value) {
                    setState(() {
                      selectedCity = value;
                    });
                  },
                ),
              ),
            ),
          ),
        ),
        Container(
          height: 16,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Container(
            height: 50,
            child: TextField(
              controller: addressController,
              cursorColor: Colors.red[600],
              decoration: InputDecoration(
                hintText: "Адрес доставки",
                fillColor: Colors.white,
                filled: true,
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey[300]),
                  borderRadius: BorderRadius.circular(25.0),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.red[600]),
                  borderRadius: BorderRadius.circular(25.0),
                ),
              ),
            ),
          ),
        ),
        Container(
          height: 16,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 16,
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                child: AppInput(
                  controller: flatController,
                  hint: 'Кв/Офис',
                  width: null,
                ),
              ),
              Container(
                width: 8,
              ),
              Expanded(
                child: AppInput(
                  controller: entranceController,
                  hint: 'Подъезд',
                ),
              ),
              Container(
                width: 8,
              ),
              Expanded(
                child: AppInput(
                  controller: codeController,
                  hint: 'Домофон',
                  width: null,
                ),
              ),
              Container(
                width: 8,
              ),
              Expanded(
                child: AppInput(
                  controller: floorController,
                  hint: 'Этаж',
                  width: null,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
