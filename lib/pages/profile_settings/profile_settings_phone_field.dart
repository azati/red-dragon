import 'package:flutter/material.dart';

import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class ProfileSettingsPhoneField extends StatefulWidget {
  final TextEditingController controller;

  const ProfileSettingsPhoneField({Key key, this.controller}) : super(key: key);

  @override
  _ProfileSettingsPhoneFieldState createState() => _ProfileSettingsPhoneFieldState();
}

class _ProfileSettingsPhoneFieldState extends State<ProfileSettingsPhoneField> {
  var maskTextInputFormatter = MaskTextInputFormatter(
    mask: "+# (###) ###-##-##",
    filter: {
      "#": RegExp(r'[0-9]'),
    },
  );

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(0.0),
      child: Container(
        height: 50,
        child: TextField(
          cursorColor: Colors.red[600],
          controller: widget.controller,
          inputFormatters: [maskTextInputFormatter],
          autocorrect: false,
          keyboardType: TextInputType.phone,
          decoration: InputDecoration(
            hintText: "+7 (800) 555-35-35",
            fillColor: Colors.white,
            filled: true,
            border: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey,
              ),
              borderRadius: BorderRadius.circular(25.0),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.red[600],
              ),
              borderRadius: BorderRadius.circular(25.0),
            ),
          ),
        ),
      ),
    );
  }
}
