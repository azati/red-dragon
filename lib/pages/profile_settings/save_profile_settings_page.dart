import 'package:flutter/material.dart';

class SaveProfileSettingsButton extends StatelessWidget {

  final Function onTap;

  const SaveProfileSettingsButton({Key key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onTap();
      },
      child: Container(
        height: 50.0,
        decoration: BoxDecoration(
          color: Colors.red[600],
          borderRadius: BorderRadius.circular(25.0),
        ),
        child: Center(
          child: Text(
            'Сохранить',
            style: TextStyle(
              fontSize: 18.0,
              fontFamily: 'PTSans',
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
