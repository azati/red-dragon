import 'package:flutter/material.dart';
import 'package:red_dragon/classes/category.dart';

class CategoryCarouselItem extends StatelessWidget {
  final Category category;
  final Function(Category) onItemClickListener;
  final bool isSelected;

  const CategoryCarouselItem({
    Key key,
    @required this.category,
    @required this.onItemClickListener,
    this.isSelected = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: InkWell(
        child: isSelected ? _buildSelectedItem() : _buildUnselectedItem(),
        onTap: () {
          onItemClickListener.call(category);
        },
      ),
    );
  }

  Widget _buildSelectedItem() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
        child: Text(
          category.name,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
            fontFamily: 'PTMono',
            color: Colors.white,
          ),
        ),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(22),
        color: Colors.red,
      ),
    );
  }

  Widget _buildUnselectedItem() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
      child: Text(
        category.name,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 20,
          fontFamily: 'PTMono',
          color: Colors.black,
        ),
      ),
    );
  }
}
