import 'package:flutter/material.dart';
import 'package:red_dragon/app_session_resources.dart';
import 'package:red_dragon/classes/category.dart';
import 'package:red_dragon/pages/menu/categoty_carousel_item.dart';

class CategoriesCarousel extends StatefulWidget {
  final Function(Category) categoryChangeListener;

  const CategoriesCarousel({
    Key key,
    @required this.categoryChangeListener,
  }) : super(key: key);

  @override
  _CategoriesCarouselState createState() => _CategoriesCarouselState();
}

class _CategoriesCarouselState extends State<CategoriesCarousel> {
  Category currentCategory = AppSessionResources.categories.first;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      color: Colors.white,
      child: ListView.builder(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(horizontal: 8),
        scrollDirection: Axis.horizontal,
        itemCount: AppSessionResources.categories.length,
        itemBuilder: (context, index) {
          return CategoryCarouselItem(
            category: AppSessionResources.categories[index],
            onItemClickListener: (category) {
              widget.categoryChangeListener.call(category);
              currentCategory = category;
              setState(() {});
            },
            isSelected: currentCategory == AppSessionResources.categories[index],
          );
        },
      ),
    );
  }
}
