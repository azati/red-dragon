import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:red_dragon/cart_repository.dart';
import 'package:red_dragon/classes/product.dart';
import 'package:red_dragon/pages/cart/cart_item_change_product_count.dart';
import 'package:red_dragon/pages/productPage/product_page.dart';
import 'package:red_dragon/widgets/cart_button.dart';

class ProductItem extends StatefulWidget {
  final Product product;

  ProductItem({@required this.product});

  @override
  _ProductItemState createState() => _ProductItemState();
}

class _ProductItemState extends State<ProductItem> {
  int count = 0;

  @override
  void initState() {
    super.initState();
    count = CartRepository.countInCart(widget.product);
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
          child: Row(
            children: [
              Expanded(
                child: Container(
                  height: 128,
                  width: 128,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: CachedNetworkImageProvider(widget.product.imageUrl),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
                flex: 2,
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.product.name,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontFamily: 'PTMono',
                          fontSize: 18,
                        ),
                        overflow: TextOverflow.ellipsis,
                      ),
                      Text(
                        widget.product.description,
                        style: TextStyle(
                          fontSize: 12,
                          fontFamily: 'PTSans',
                          color: Colors.grey[700],
                        ),
                      ),
                      Container(
                        height: 8,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          if (widget.product.weight != null)
                            Text(
                              '${widget.product.weight} г',
                              style: TextStyle(
                                fontSize: 12,
                                fontFamily: 'PTSans',
                                color: Colors.grey[700],
                              ),
                            ),
                          if (widget.product.quantity != null)
                            Text(
                              '${widget.product.quantity} шт',
                              style: TextStyle(
                                fontSize: 12,
                                fontFamily: 'PTSans',
                                color: Colors.grey[700],
                              ),
                            ),
                          if (widget.product.calorie != null)
                            Text(
                              '${widget.product.calorie} ккал',
                              style: TextStyle(
                                fontSize: 12,
                                fontFamily: 'PTSans',
                                color: Colors.grey[700],
                              ),
                            ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            '${widget.product.price}₽',
                            style: TextStyle(
                              fontFamily: 'PTSans',
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                          ),
                          CartRepository.alreadyInCart(widget.product)
                              ? CartItemChangeProductCount(
                                  changeListener: (add) {
                                    if (count + add >= 0) {
                                      count += add;
                                      if (add == 1) {
                                        CartRepository.addProductToCart(widget.product, 1);
                                      } else {
                                        if (count + add == 0) {
                                          CartRepository.removeProductFromCart(widget.product);
                                        } else {
                                          CartRepository.removeProductFromCartOnce(widget.product);
                                        }
                                      }
                                    }
                                    setState(() {});
                                  },
                                  initialQuantity: CartRepository.countInCart(widget.product),
                                )
                              : CartButton(
                                  onTap: () {
                                    CartRepository.addProductToCart(widget.product, 1);
                                    count = 1;
                                    setState(() {});
                                  },
                                )
                        ],
                      ),
                    ],
                  ),
                ),
                flex: 3,
              )
            ],
          ),
        ),
      ),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            fullscreenDialog: true,
            builder: (context) =>
                ProductPage(
                  product: widget.product,
                  updateListener: () {
                    setState(() {});
                  },
                ),
          ),
        );
      },
    );
  }
}
