import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:red_dragon/app_session_resources.dart';
import 'package:red_dragon/classes/category.dart';
import 'package:red_dragon/classes/product.dart';

class MenuApi {
  static Future<List<Category>> getCategories() async {
    final endpoint = 'https://rd-sushi.ru/api/menus/index';
    final response = await http.get(endpoint);
    if (response.statusCode == 200) {
      List<dynamic> json = jsonDecode(response.body);
      List<Category> categories = [];
      for (int i = 0; i < json.length; i++) {
        categories.add(Category.fromJson(json[i]));
      }
      return categories;
    } else {
      throw Exception('getCategories response code is not 200');
    }
  }

  static Future<List<Product>> getProducts(int categoryId) async {
    final endpoint = 'https://rd-sushi.ru/api/menus/view/$categoryId';
    final response = await http.get(
      endpoint,
    );
    if (response.statusCode == 200) {
      List<dynamic> json = jsonDecode(response.body);
      List<Product> products = [];
      for (int i = 0; i < json.length; i++) {
        final product = Product.fromJson(json[i]);
        products.add(product);
      }
      return products;
    } else {
      throw Exception('getProducts response code is not 200');
    }
  }

  static Future<List<Product>> getAdditionalProducts() async {
    final endpoint = 'https://rd-sushi.ru/api/menus/dop';
    final response = await http.get(
      endpoint,
    );
    if (response.statusCode == 200) {
      List<dynamic> json = jsonDecode(response.body);
      List<Product> products = [];
      for (int i = 0; i < json.length; i++) {
        final product = Product.fromJson(json[i]);
        products.add(product);
      }
      return products;
    } else {
      throw Exception('getAdditionalProducts response code is not 200');
    }
  }
}
