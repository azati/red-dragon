import 'package:flutter/material.dart';
import 'package:red_dragon/app_session_resources.dart';
import 'package:red_dragon/classes/category.dart';
import 'package:red_dragon/classes/product.dart';
import 'package:red_dragon/pages/auth/auth_repository.dart';
import 'package:red_dragon/pages/menu/product_item.dart';
import 'package:red_dragon/pages/menu/categories_carousel.dart';

class MenuMainPage extends StatefulWidget {
  @override
  _MenuMainPageState createState() => _MenuMainPageState();
}

class _MenuMainPageState extends State<MenuMainPage> {
  Category currentCategory;
  List<Product> currentProducts;

  @override
  void initState() {
    super.initState();
    if (AppSessionResources.categories.length > 0) {
      currentCategory = AppSessionResources.categories[0];
    }
    AuthRepository.authController.stream.listen((event) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(image: AssetImage('assets/background.png'), fit: BoxFit.cover),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          currentCategory != null
              ? CategoriesCarousel(
                  categoryChangeListener: (category) {
                    setState(
                      () {
                        currentProducts = null;
                        currentCategory = category;
                      },
                    );
                  },
                )
              : Container(),
          Expanded(
            child: SingleChildScrollView(
              primary: false,
              child: _buildProducts(),
            ),
          ),
        ],
      ),
    );
  }

  void _onProductsLoaded(List<Product> products) {
    setState(() {
      this.currentProducts = products;
    });
  }

  Widget _buildProducts() {
    if (currentProducts == null && currentCategory == null) {
      return Center(
        child: Padding(
          padding: const EdgeInsets.all(32.0),
          child: CircularProgressIndicator(),
        ),
      );
    } else if (currentProducts == null) {
      AppSessionResources.getProducts(currentCategory).then((products) => _onProductsLoaded(products));
      return Center(
        child: Padding(
          padding: const EdgeInsets.all(32.0),
          child: CircularProgressIndicator(),
        ),
      );
    } else {
      return Container(
        color: Colors.white,
        child: ListView.separated(
          primary: false,
          itemBuilder: (context, index) {
            return ProductItem(
              product: currentProducts[index],
            );
          },
          separatorBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Container(
                height: 1,
                color: Color(0xFFF7F7F7),
              ),
            );
          },
          itemCount: currentProducts.length,
          shrinkWrap: true,
        ),
      );
    }
  }
}
