import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:red_dragon/classes/order_history.dart';
import 'package:red_dragon/classes/order_history_product.dart';
import 'package:red_dragon/pages/orderHistoryList/order_history_details_page.dart';

class OrderHistoryItem extends StatelessWidget {
  final OrderHistory orderHistory;

  const OrderHistoryItem({
    Key key,
    @required this.orderHistory,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: InkWell(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      DateFormat('dd.').format(orderHistory.date) +
                          DateFormat('MM.').format(orderHistory.date) +
                          DateFormat('yy').format(orderHistory.date) +
                          DateFormat(' hh:mm').format(orderHistory.date),
                      style: TextStyle(fontFamily: 'PTMono', fontWeight: FontWeight.normal, fontSize: 16),
                    ),
                    Text(
                      '${orderHistory.orderSum} ₽',
                      style: TextStyle(fontFamily: 'PTSans', fontWeight: FontWeight.bold, fontSize: 20),
                    )
                  ],
                ),
                SizedBox(
                  height: 16,
                ),
                ListView.builder(
                  primary: false,
                  shrinkWrap: true,
                  itemBuilder: (context, index) => ProductHistoryItem(
                    product: orderHistory.products[index],
                  ),
                  itemCount: orderHistory.products.length,
                ),
              ],
            ),
          ),
        ),
        onTap: (){
          Navigator.push(context,
            MaterialPageRoute(builder: (context) =>
                OrderHistoryDetailsPage(
                  orderHistory: orderHistory,
                ),),
          );
        },
      ),
    );
  }
}


class ProductHistoryItem extends StatelessWidget {
  final OrderHistoryProduct product;

  const ProductHistoryItem({
    Key key,
    @required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            '* ${product.name}',
            style: TextStyle(fontFamily: 'PTMono', fontWeight: FontWeight.normal, fontSize: 16),
          ),
          Text(
            '${product.quantity} * ${product.price}',
            style: TextStyle(
                fontFamily: 'PTMono',
                fontWeight: FontWeight.normal,
                fontSize: 16
            ),
          )
        ],
      ),
    );
  }
}
