import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:red_dragon/cart_repository.dart';
import 'package:red_dragon/classes/order_history.dart';
import 'package:red_dragon/pages/confirmOrder/confirm_order_page.dart';
import 'package:red_dragon/pages/orderHistoryList/order_history_item.dart';
import 'package:red_dragon/widgets/app_scaffold.dart';
import 'package:red_dragon/widgets/phone_button.dart';

class OrderHistoryDetailsPage extends StatelessWidget {
  final OrderHistory orderHistory;

  const OrderHistoryDetailsPage({
    Key key,
    @required this.orderHistory,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      appBarTitle: 'Заказ',
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    DateFormat('dd.').format(orderHistory.date) +
                        DateFormat('MM.').format(orderHistory.date) +
                        DateFormat('yy').format(orderHistory.date) +
                        DateFormat(' hh:mm').format(orderHistory.date),
                    style: TextStyle(
                      fontFamily: 'PTMono',
                      fontWeight: FontWeight.normal,
                      fontSize: 16,
                    ),
                  ),
                  Text(
                    '${orderHistory.orderSum} ₽',
                    style: TextStyle(
                      fontFamily: 'PTSans',
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 16,
              ),
              ListView.builder(
                primary: false,
                shrinkWrap: true,
                itemBuilder: (context, index) => ProductHistoryItem(
                  product: orderHistory.products[index],
                ),
                itemCount: orderHistory.products.length,
              ),
              SizedBox(
                height: 16,
              ),
              RepeatButton(
                orderHistory: orderHistory,
              )
            ],
          ),
        ),
      ),
    );
  }
}

class RepeatButton extends StatelessWidget {

  final OrderHistory orderHistory;

  const RepeatButton({Key key, @required this.orderHistory,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        CartRepository.clear();
        CartRepository.fill(orderHistory);
        Navigator.pop(context);
        Navigator.pop(context);
        Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => ConfirmOrderPage(orderHistory: orderHistory),),);
      },
      child: Container(
        height: 50.0,
        decoration: BoxDecoration(
          color: Colors.red[600],
          borderRadius: BorderRadius.circular(25.0),
        ),
        child: Center(
          child: Text(
            'Повторить',
            style: TextStyle(
              fontSize: 18.0,
              fontFamily: 'PTSans',
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
