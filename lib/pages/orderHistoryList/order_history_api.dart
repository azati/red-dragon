import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:red_dragon/app_session_resources.dart';
import 'package:red_dragon/classes/order_history.dart';

class OrderHistoryApi {
  static Future<List<OrderHistory>> getOrderHistory() async {
    final endpoint = 'https://rd-sushi.ru/api/users/view';
    final token = AppSessionResources.token;
    final headers = {
      'Authorization': 'Bearer $token',
    };
    final response = await http.get(
      endpoint,
      headers: headers,
    );
    if (response.statusCode == 200) {
      Map json = jsonDecode(response.body);
      List<OrderHistory> orders = [];
      List values = json.values.toList();
      for (int i = 0; i < values.length; i++) {
        orders.add(OrderHistory.fromJson(values[i]));
      }
      return orders;
    } else {
      throw Exception('getOrderHistory response code is not 200');
    }
  }
}
