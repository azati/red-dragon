import 'package:flutter/material.dart';
import 'package:red_dragon/classes/order_history.dart';
import 'package:red_dragon/pages/cart/buy_products_button.dart';
import 'package:red_dragon/pages/main/main_navigator.dart';
import 'package:red_dragon/pages/orderHistoryList/order_history_item.dart';
import 'package:red_dragon/pages/orderHistoryList/order_history_api.dart';
import 'package:red_dragon/widgets/app_scaffold.dart';

class OrdersHistoryPage extends StatefulWidget {
  @override
  _OrdersHistoryPageState createState() => _OrdersHistoryPageState();
}

class _OrdersHistoryPageState extends State<OrdersHistoryPage> {
  List<OrderHistory> orders;

  @override
  void initState() {
    super.initState();
    OrderHistoryApi.getOrderHistory().then((orders) => _onSuccess(orders)).catchError((error) => _onError(error));
  }

  void _onSuccess(List<OrderHistory> orders) {
    setState(() {
      this.orders = orders;
    });
  }

  void _onError(Exception e) {
    print(e);
    setState(() {
      this.orders = [];
    });
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      appBarTitle: 'История заказов',
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    if (orders == null) {
      return Padding(
        padding: const EdgeInsets.all(32.0),
        child: CircularProgressIndicator(),
      );
    } else if (orders.length == 0) {
      return Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              'Вы не совершили еще ни одного заказа',
              style: TextStyle(
                fontFamily: 'PTMono',
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.center,
            ),
            Container(
              height: 16,
            ),
            AppButton(
              title: 'Сделать заказ',
              onTap: () {
                Navigator.of(context).pop();
                MainNavigator.navigateToIndex(0);
              },
            )
          ],
        ),
      );
    } else {
      return ListView.separated(
        primary: false,
        itemBuilder: (context, index) {
          return OrderHistoryItem(
            orderHistory: orders[index],
          );
        },
        separatorBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Container(
              height: 1,
              color: Colors.grey[300],
            ),
          );
        },
        itemCount: orders.length,
        shrinkWrap: true,
      );
    }
  }
}
