import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:red_dragon/app_session_resources.dart';
import 'package:red_dragon/cart_repository.dart';
import 'package:red_dragon/classes/OrderCity.dart';
import 'package:red_dragon/classes/order_details.dart';
import 'package:red_dragon/classes/order_history.dart';
import 'package:red_dragon/classes/order_product.dart';
import 'package:red_dragon/classes/payment_type.dart';
import 'package:red_dragon/pages/cart/buy_products_button.dart';
import 'package:red_dragon/pages/confirmOrder/order_api.dart';
import 'package:red_dragon/pages/confirmOrder/page_title_item.dart';
import 'package:red_dragon/pages/confirmOrder/widgets/bonus_widget.dart';
import 'package:red_dragon/pages/confirmOrder/widgets/outlet_widget.dart';
import 'package:red_dragon/pages/confirmOrder/widgets/payment_type_widget.dart';
import 'package:red_dragon/pages/confirmOrder/widgets/promocode_widget.dart';
import 'package:red_dragon/pages/confirmOrder/widgets/ready_time_widget.dart';
import 'package:red_dragon/pages/order_accepted/order_accepted_page.dart';
import 'package:red_dragon/widgets/app_input.dart';
import 'package:red_dragon/widgets/app_scaffold.dart';
import 'package:red_dragon/widgets/helper.dart';

class ConfirmOrderPage extends StatefulWidget {
  final OrderHistory orderHistory;

  const ConfirmOrderPage({
    Key key,
    this.orderHistory,
  }) : super(key: key);

  @override
  _ConfirmOrderPageState createState() => _ConfirmOrderPageState();
}

class _ConfirmOrderPageState extends State<ConfirmOrderPage> {
  bool isOutlet = false;

  final TextEditingController addressController = TextEditingController();

  final TextEditingController officeController = TextEditingController();

  final TextEditingController entranceController = TextEditingController();

  final TextEditingController doorCodeController = TextEditingController();

  final TextEditingController floorController = TextEditingController();

  final TextEditingController commentController = TextEditingController();

  final TextEditingController promoCodeController = TextEditingController();
  final TextEditingController bonusController = TextEditingController();

  final int orderPrice = CartRepository.getCartProductPrice();

  String appliedPromocode;
  int appliedBonuses;
  PaymentType paymentType = PaymentType.CASH;
  DateTime selectedDateTime = DateTime.fromMicrosecondsSinceEpoch(0);
  OrderCity selectedCity;

  @override
  void initState() {
    super.initState();
    if (widget.orderHistory != null) {
      final orderHistory = widget.orderHistory;
      isOutlet = orderHistory.type == 0 ? true : false;
      addressController.text = orderHistory.address;
      officeController.text = orderHistory.office;
      entranceController.text = orderHistory.entrance;
      doorCodeController.text = orderHistory.doorphone;
      floorController.text = orderHistory.floor;
      commentController.text = orderHistory.text;
      selectedCity = AppSessionResources.tryFindCity(orderHistory.city);
    }
  }

  @override
  void dispose() {
    if (widget.orderHistory != null) {
      CartRepository.clear();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      appBarTitle: 'Оплата',
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [
                  Expanded(
                    child: PageTitleItem(
                      title: 'Доставка',
                      onItemClickListener: () {
                        isOutlet = false;
                        setState(() {});
                      },
                      isSelected: !isOutlet,
                    ),
                  ),
                  Expanded(
                    child: PageTitleItem(
                      title: 'Самовывоз',
                      onItemClickListener: () {
                        isOutlet = true;
                        setState(() {});
                      },
                      isSelected: isOutlet,
                    ),
                  ),
                ],
              ),
            ),
            isOutlet ? OutletWidget() : _buildDeliveryWidget(),
            _buildCommonWidget(),
            _buildBottomWidget(),
          ],
        ),
      ),
    );
  }

  Widget _buildDeliveryWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 16,
              ),
              child: Text(
                'Адрес доставки',
                style: TextStyle(
                  fontFamily: 'PTMono',
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
        Container(
          padding: const EdgeInsets.only(top: 4.0, left: 16.0, right: 16.0),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(30.0),
            border: Border.all(color: Colors.grey),
          ),
          child: Center(
            child: DropdownButtonHideUnderline(
              child: DropdownButton<OrderCity>(
                isExpanded: true,
                style: TextStyle(
                  fontFamily: 'PTMono',
                  fontSize: 18.0,
                ),
                value: selectedCity,
                hint: Text('Город'),
                items: AppSessionResources.cities.map((OrderCity city) {
                  return DropdownMenuItem(
                    value: city,
                    child: Text(
                      city.name,
                      style: TextStyle(
                        fontFamily: 'PTMono',
                        color: Colors.black,
                      ),
                    ),
                  );
                }).toList(),
                onChanged: (OrderCity value) {
                  selectedCity = value;
                  setState(() {});
                },
              ),
            ),
          ),
        ),
        Container(
          height: 16,
        ),
        AppInput(
          controller: addressController,
          hint: "Адрес доставки",
          width: null,
        ),
        Container(
          height: 16,
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: AppInput(
                controller: officeController,
                hint: 'Кв/Офис',
                width: null,
              ),
            ),
            Container(
              width: 8,
            ),
            Expanded(
              child: AppInput(
                controller: entranceController,
                hint: 'Подъезд',
                width: null,
              ),
            ),
            Container(
              width: 8,
            ),
            Expanded(
              child: AppInput(
                controller: doorCodeController,
                hint: 'Домофон',
                width: null,
              ),
            ),
            Container(
              width: 8,
            ),
            Expanded(
              child: AppInput(
                controller: floorController,
                hint: 'Этаж',
                width: null,
              ),
            ),
          ],
        )
      ],
    );
  }

  Widget _buildCommonWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 16,
          ),
          child: Text(
            'Комментарий',
            style: TextStyle(
              fontFamily: 'PTMono',
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        AppInput(
          controller: commentController,
          hint: 'Комментарий',
          width: null,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 16,
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    PaymentTypeWidget(
                      paymentType: paymentType,
                      onChanged: (value) {
                        setState(() {
                          paymentType = value;
                        });
                      },
                    ),
                  ],
                ),
              ),
              Container(
                width: 8,
              ),
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    ReadyTimeWidget(
                      dateTime: selectedDateTime,
                      onChanged: (value) {
                        setState(() {
                          selectedDateTime = value;
                        });
                      },
                      title: isOutlet ? 'Когда заберете?' : 'Время доставки',
                    )
                  ],
                ),
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(
            bottom: 16,
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                child: PromoCodeWidget(
                  controller: promoCodeController,
                  onConfirm: () async {
                    showLoadingAlert(context);
                    OrderApi.checkCode(code: promoCodeController.text).then((value) {
                      Navigator.of(context).pop();
                      if (value) {
                        Fluttertoast.showToast(
                          msg: "Код применен",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIosWeb: 1,
                        );
                        appliedPromocode = promoCodeController.text;
                      } else {
                        Fluttertoast.showToast(
                          msg: "Недействительный код",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIosWeb: 1,
                        );
                      }
                    }).catchError((error) {
                      Navigator.of(context).pop();
                      showAlert(context, 'Ошибка', 'Не удалось проверить правильность кода.');
                    });
                  },
                ),
              ),
              Container(
                width: 8,
              ),
              Expanded(
                child: BonusWidget(
                  controller: bonusController,
                  onConfirm: () {
                    final availableBonuses = AppSessionResources.user.bonusSum;
                    if (bonusController.text != null && bonusController.text != null) {
                      final enteredBonuses = int.tryParse(bonusController.text) ?? 0;
                      if (enteredBonuses == 0) {
                        return;
                      }
                      if (enteredBonuses > availableBonuses) {
                        showAlert(context, 'Ошибка',
                            'Недостаточно бонусов. Сейчас у вас на счету $availableBonuses бонусов.');
                      } else {
                        Fluttertoast.showToast(
                          msg: "Стоимость заказа пересчитана с учетом бонусов",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIosWeb: 1,
                        );
                        this.appliedBonuses = int.tryParse(bonusController.text.toString());
                        setState(() {});
                      }
                    }
                  },
                ),
              )
            ],
          ),
        ),
        Row(
          children: [
            Expanded(
              child: Text('Стоимость'),
            ),
            Text(CartRepository.getCartProductPrice().toString() + ' Р'),
          ],
        ),
      ],
    );
  }

  Widget _buildAppliedBonusesWidget() {
    if (appliedBonuses == null || appliedBonuses == 0) {
      return Container();
    } else {
      return Row(
        children: [
          Expanded(
            child: Text(
              'Примененные бонусы:',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Text(
            '$appliedBonuses P',
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          )
        ],
      );
    }
  }

  Widget _buildBottomWidget() {
    if (!isOutlet && selectedCity == null) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16),
            child: Text(
              'Окончательная стоимость будет рассчитана после выбара города доставки',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.red,
                fontSize: 20,
              ),
            ),
          )
        ],
      );
    } else {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          isOutlet
              ? Container()
              : _buildDeliveryPriceWidget(selectedCity.freeDeliveryPrice, selectedCity.deliveryPrice),
          _buildAppliedBonusesWidget(),
          Row(
            children: [
              Expanded(
                child: Text(
                  'Итого:',
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Text(
                '${isOutlet ? _getFinalPriceWithOutlet(orderPrice: orderPrice) : _getFinalPriceWithDelivery(orderPrice: orderPrice, minFreeDeliveryPrice: selectedCity?.freeDeliveryPrice ?? 0, deliveryPrice: selectedCity?.deliveryPrice ?? 0)} P',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 16,
            ),
            child: AppButton(
              title: 'Оформить',
              onTap: () async {
                if (isOutlet) {
                  _makeOrder();
                } else {
                  if (validateImportantFields()) {
                    _makeOrder();
                  }
                }
              },
            ),
          )
        ],
      );
    }
  }

  Widget _buildDeliveryPriceWidget(int minFreeDeliveryPrice, int deliveryPrice) {
    if (minFreeDeliveryPrice == 0) {
      return Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 8,
        ),
        child: Row(
          children: [
            Expanded(
              child: Text(
                'Доставка',
                style: TextStyle(
                  fontFamily: 'PTSans',
                ),
              ),
            ),
            Text("$deliveryPrice P"),
          ],
        ),
      );
    }
    if (minFreeDeliveryPrice > CartRepository.getCartProductPrice()) {
      return Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 8,
        ),
        child: Row(
          children: [
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Доставка',
                    style: TextStyle(
                      fontFamily: 'PTSans',
                    ),
                  ),
                  Text(
                    'Закажите еще на ${minFreeDeliveryPrice - CartRepository.getCartProductPrice()} P для бесплатной доствки',
                    style: TextStyle(
                      color: Colors.red,
                      fontSize: 10,
                    ),
                  )
                ],
              ),
            ),
            Text("$deliveryPrice P"),
          ],
        ),
      );
    } else {
      return Container();
    }
  }

  void _makeOrder() {
    showLoadingAlert(context);
    OrderApi.makeOrder(
      orderDetails: OrderDetails.formData(
        orderType: isOutlet ? 0 : 1,
        orderCity: isOutlet ? 'Нальчик' : selectedCity.name,
        orderAddress: addressController.text,
        orderOffice: officeController.text,
        orderEntrance: entranceController.text,
        orderDoorPhone: doorCodeController.text,
        orderFloor: floorController.text,
        comment: commentController.text,
        paymentType: paymentType == PaymentType.CASH ? 0 : 1,
        orderReadyTime: selectedDateTime,
        promocode: appliedPromocode,
        usedBonuses: appliedBonuses,
        productsPrice: CartRepository.getCartProductPrice(),
        deliveryPrice: isOutlet
            ? 0
            : _getDeliveryPrice(
                orderPrice: orderPrice,
                minFreeDeliveryPrice: selectedCity.freeDeliveryPrice,
                deliveryPrice: selectedCity.deliveryPrice,
              ),
        finalPrice: isOutlet
            ? _getFinalPriceWithOutlet(orderPrice: orderPrice)
            : _getFinalPriceWithDelivery(
                orderPrice: orderPrice,
                minFreeDeliveryPrice: selectedCity.freeDeliveryPrice,
                deliveryPrice: selectedCity.deliveryPrice,
              ),
        products: CartRepository.getProducts()
            .map(
              (e) => OrderProduct(
                quantity: e.quantity,
                productId: e.product.id,
              ),
            )
            .toList(),
      ),
    ).then((success) {
      Navigator.of(context).pop();
      Navigator.of(context).pop();
      if (success) {
        CartRepository.clear();
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => OrderAcceptedPage(),
          ),
        );
      } else {
        Navigator.of(context).pop();
        Fluttertoast.showToast(
          msg: "Не удалось сформировать заказ",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
        );
        print('failed too');
      }
    }).catchError((e) {
      Navigator.of(context).pop();
      Fluttertoast.showToast(
        msg: "Не удалось сформировать заказ",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
      );
      print('failed');
    });
  }

  bool validateImportantFields() {
    final address = addressController.text;
    final office = officeController.text;
    if (address == null || address == "") {
      showAlert(context, 'Ошибка', 'Поле "Адрес доставки" не может быть пуст.');
      return false;
    }
    if (office == null || office == "") {
      showAlert(context, 'Ошибка', 'Поле "Кв/офис" не может быть пуст.');
      return false;
    }
    return true;
  }

  int _getDeliveryPrice({
    int orderPrice,
    int minFreeDeliveryPrice,
    int deliveryPrice,
  }) {
    if (minFreeDeliveryPrice == 0) {
      return deliveryPrice;
    }
    if (orderPrice >= minFreeDeliveryPrice) {
      return 0;
    } else {
      return deliveryPrice;
    }
  }

  int _getFinalPriceWithDelivery({
    int orderPrice,
    int minFreeDeliveryPrice,
    int deliveryPrice,
  }) {
    int bonuses = appliedBonuses ?? 0;
    if (minFreeDeliveryPrice == 0) {
      return orderPrice + deliveryPrice - bonuses;
    } else if (orderPrice >= minFreeDeliveryPrice) {
      return orderPrice - bonuses;
    } else {
      return orderPrice + deliveryPrice - bonuses;
    }
  }

  int _getFinalPriceWithOutlet({int orderPrice}) {
    int bonuses = appliedBonuses ?? 0;
    return orderPrice - bonuses;
  }
}
