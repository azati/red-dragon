import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:red_dragon/app_session_resources.dart';
import 'package:red_dragon/classes/order_details.dart';
import 'package:http/http.dart' as http;
import 'package:red_dragon/classes/promotion.dart';

class OrderApi {
  static Future<bool> makeOrder({@required OrderDetails orderDetails}) async {
    final endpoint = 'https://rd-sushi.ru/api/users/send-order';
    final body = jsonEncode(orderDetails);
    final headers = {
      'Authorization': 'Bearer ${AppSessionResources.token}',
      'Content-Type': 'application/json',
    };
    final response = await http.post(
      endpoint,
      headers: headers,
      body: body,
    );
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  static Future<bool> checkCode({@required String code}) async {
    final endpoint = 'https://rd-sushi.ru/api/stocks/check';
    final promotion = AppSessionResources.promotions.firstWhere((element) => element.code == code, orElse: () {
      return Promotion.empty();
    });
    if (promotion.id == null) {
      return false;
    } else {
      final body = {
        'id': promotion.id.toString(),
        'code': promotion.code.toString(),
      };
      final response = await http.post(endpoint, body: body);
      if (response.statusCode == 200) {
        dynamic json = jsonDecode(response.body);
        return true;
      } else {
        throw Exception('checkCode response code is not 200');
      }
    }
  }
}
