import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:red_dragon/widgets/app_input.dart';
import 'package:red_dragon/widgets/app_input_with_suffix.dart';

class BonusWidget extends StatelessWidget {
  final TextEditingController controller;
  final Function onConfirm;

  const BonusWidget({
    Key key,
    @required this.controller,
    @required this.onConfirm,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          'Списать бонусы?',
          style: TextStyle(
            fontFamily: 'PTMono',
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
        ),
        Container(
          height: 8,
        ),
        AppInputWithSuffix(
          controller: controller,
          hint: 'Бонусы',
          inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
          onTap: () {
            onConfirm.call();
          },
        ),
      ],
    );
  }
}
