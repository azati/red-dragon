import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:red_dragon/classes/payment_type.dart';

class PaymentTypeWidget extends StatefulWidget {
  final Function(PaymentType) onChanged;
  final PaymentType paymentType;

  const PaymentTypeWidget({
    Key key,
    @required this.onChanged,
    @required this.paymentType,
  }) : super(key: key);

  @override
  _PaymentTypeWidgetState createState() => _PaymentTypeWidgetState();
}

class _PaymentTypeWidgetState extends State<PaymentTypeWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          'Способ оплаты',
          style: TextStyle(
            fontFamily: 'PTMono',
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
        ),
        Container(
          height: 8,
        ),
        Container(
          padding: const EdgeInsets.only(top: 4.0, left: 16.0, right: 16.0),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(30.0),
            border: Border.all(color: Colors.grey),
          ),
          child: Center(
            child: DropdownButtonHideUnderline(
              child: DropdownButton(
                style: TextStyle(
                  fontFamily: 'PTMono',
                  fontSize: 18.0,
                ),
                value: widget.paymentType,
                hint: Text('Город'),
                items: [
                  DropdownMenuItem(
                    value: widget.paymentType,
                    child: Text(
                      getPaymentTypeTitle(widget.paymentType),
                      style: TextStyle(
                        fontFamily: 'PTMono',
                        color: Colors.black,
                      ),
                    ),
                  )
                ],
                onChanged: (PaymentType value) {
                  widget.onChanged.call(value);
                  setState(() {

                  });
                },
              ),
            ),
          ),
        ),
      ],
    );
  }
}
