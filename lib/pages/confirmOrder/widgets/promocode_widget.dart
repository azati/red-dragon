import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:red_dragon/widgets/app_input.dart';
import 'package:red_dragon/widgets/app_input_with_suffix.dart';

class PromoCodeWidget extends StatelessWidget {
  final TextEditingController controller;
  final Function onConfirm;

  const PromoCodeWidget({
    Key key,
    @required this.controller,
    @required this.onConfirm,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          'Промокод',
          style: TextStyle(
            fontFamily: 'PTMono',
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
        ),
        Container(
          height: 8,
        ),
        AppInputWithSuffix(
          controller: controller,
          hint: 'Код',
          onTap: () {
            onConfirm.call();
          },
        ),
      ],
    );
  }
}
