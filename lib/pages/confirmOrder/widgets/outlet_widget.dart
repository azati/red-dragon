import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:red_dragon/app_session_resources.dart';
import 'package:red_dragon/widgets/app_input.dart';

class OutletWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 16, 0, 16),
          child: Text(
            'Пункт вывоза',
            style: TextStyle(
              fontFamily: 'PTMono',
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        AppInput.disabled(
          hint: AppSessionResources.contacts.address,
        ),
      ],
    );
  }
}
