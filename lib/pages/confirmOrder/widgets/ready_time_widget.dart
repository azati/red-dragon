import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ReadyTimeWidget extends StatefulWidget {
  final DateTime dateTime;
  final Function(DateTime) onChanged;
  final String title;

  const ReadyTimeWidget({
    Key key,
    @required this.dateTime,
    @required this.onChanged,
    @required this.title,
  }) : super(key: key);

  @override
  _ReadyTimeWidgetState createState() => _ReadyTimeWidgetState();
}

class _ReadyTimeWidgetState extends State<ReadyTimeWidget> {
  List<DateTime> times;
  DateTime selectedDateTime;

  @override
  void initState() {
    super.initState();
    times = _generateTimes(widget.dateTime);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          widget.title,
          style: TextStyle(
            fontFamily: 'PTMono',
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
        ),
        Container(
          height: 8,
        ),
        Container(
          padding: const EdgeInsets.only(
            top: 4.0,
            left: 16.0,
            right: 16.0,
          ),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(30.0),
            border: Border.all(
              color: Colors.grey,
            ),
          ),
          child: Center(
            child: DropdownButtonHideUnderline(
              child: DropdownButton(
                style: TextStyle(
                  fontFamily: 'PTMono',
                  fontSize: 18.0,
                ),
                value: selectedDateTime,
                hint: Text('Время'),
                items: _generateDropDownItems(times),
                onChanged: (DateTime value) {
                  selectedDateTime = value;
                  widget.onChanged.call(value);
                  setState(() {

                  });
                },
              ),
            ),
          ),
        ),
      ],
    );
  }

  String _getTimeHHMM(DateTime dateTime) {
    return DateFormat('HH:mm').format(dateTime);
  }

  List<DateTime> _generateTimes(DateTime excludeTime) {
    DateTime now = DateTime.now();
    if (now.minute % 15 != 0) {
      int diff = now.minute % 15;
      int addition = 15 - diff;
      now = now.add(Duration(minutes: addition + 120));
    }
    final List<DateTime> times = [];
    while (true) {
      if (now.hour >= 23 && now.minute > 0 || now.hour < 11) {
        break;
      }
      final DateTime time = DateTime.fromMillisecondsSinceEpoch(now.millisecondsSinceEpoch);
      now = now.add(Duration(minutes: 15));
      if (excludeTime != null && now.hour == excludeTime.hour && now.minute == excludeTime.minute) {
        continue;
      } else {
        times.add(time);
      }
    }
    return times;
  }

  List<DropdownMenuItem<DateTime>> _generateDropDownItems(List<DateTime> times) {
    List<DropdownMenuItem<DateTime>> items = times
        .map(
          (e) => DropdownMenuItem(
            value: e,
            child: Text(
              _getTimeHHMM(e),
              style: TextStyle(
                fontFamily: 'PTMono',
                color: Colors.black,
              ),
            ),
          ),
        )
        .toList();
    DropdownMenuItem<DateTime> firstItem = DropdownMenuItem(
      value: DateTime.fromMillisecondsSinceEpoch(0),
      child: Text(
        'Побыстрее',
        style: TextStyle(
          fontFamily: 'PTMono',
          color: Colors.black,
        ),
      ),
    );
    items.insert(0, firstItem);
    return items;
  }
}
