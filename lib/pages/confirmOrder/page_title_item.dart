import 'package:flutter/material.dart';

class PageTitleItem extends StatelessWidget {
  final Function() onItemClickListener;
  final String title;
  final bool isSelected;

  const PageTitleItem({
    Key key,
    @required this.onItemClickListener,
    this.isSelected = false,
    @required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: InkWell(
        child: isSelected ? _buildSelectedItem() : _buildUnselectedItem(),
        onTap: () {
          onItemClickListener.call();
        },
      ),
    );
  }

  Widget _buildSelectedItem() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 16,
          vertical: 8,
        ),
        child: Text(
          title,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
            fontFamily: 'PTMono',
            color: Colors.white,
          ),
        ),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(22),
        color: Colors.red,
      ),
    );
  }

  Widget _buildUnselectedItem() {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 16,
        vertical: 8,
      ),
      child: Text(
        title,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 20,
          fontFamily: 'PTMono',
          color: Colors.black,
        ),
      ),
    );
  }
}
