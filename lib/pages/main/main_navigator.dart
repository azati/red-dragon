import 'dart:async';

class MainNavigator {
  static StreamController navigationStream = StreamController<int>();

  static void navigateToPage(NavigationPage page) {
    navigationStream.add(getPageIndex(page));
  }

  static void navigateToIndex(int index) {
    navigationStream.add(index);
  }

  static int getPageIndex(NavigationPage page) {
    switch (page) {
      case NavigationPage.MENU:
        return 0;
        break;
      case NavigationPage.PROMOTIONS:
        return 1;
        break;
      case NavigationPage.PROFILE:
        return 2;
        break;
      case NavigationPage.CART:
        return 3;
        break;
    }
    return 0;
  }
}

enum NavigationPage { MENU, PROMOTIONS, PROFILE, CART }
