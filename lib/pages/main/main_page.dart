import 'package:flutter/material.dart';
import 'package:red_dragon/cart_repository.dart';
import 'package:red_dragon/pages/cart/cart_page.dart';
import 'package:red_dragon/pages/main/main_navigator.dart';
import 'package:red_dragon/pages/menu/menu_page.dart';
import 'package:red_dragon/pages/profile/profile_page.dart';
import 'package:red_dragon/pages/promotions/promotion_list_widget.dart';
import 'package:red_dragon/widgets/app_scaffold.dart';
import 'package:red_dragon/widgets/phone_button.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> implements MainNavigator {
  Stream navigationStream = MainNavigator.navigationStream.stream;

  int _selectedIndex = 0;
  List<Widget> _widgetOptions = <Widget>[
    MenuMainPage(),
    PromotionsMainPage(),
    ProfileMainPage(),
    CartMainPage(),
  ];

  @override
  void initState() {
    super.initState();
    CartRepository.productInCartStream.listen((products) {
      setState(() {});
    });
    navigationStream.listen((pageIndex) {
      setState(() {
        _selectedIndex = pageIndex;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    List<String> titles = ['Меню', 'Акции', 'Профиль', 'Корзина'];

    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        elevation: 2.0,
        leading: Image.asset(
          'assets/logo3.png',
        ),
        actions: [
          PhoneButton(),
        ],
        title: Text(
          titles.elementAt(_selectedIndex),
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: 18,
            fontFamily: 'PTSans',
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: _widgetOptions.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: ImageIcon(
              AssetImage('assets/logo.png'),
            ),
            title: Text('Меню'),
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(
              AssetImage('assets/saleIcon.png'),
            ),
            title: Text('Акции'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text('Профиль'),
          ),
          BottomNavigationBarItem(
            icon: Stack(
              children: [
                Icon(Icons.shopping_cart),
                CartRepository.differentProductsInCart() > 0
                    ? Positioned(
                        top: 0,
                        right: 0,
                        child: Container(
                          width: 13,
                          height: 13,
                          decoration: BoxDecoration(
                            color: Colors.red[600],
                            borderRadius: BorderRadius.all(
                              Radius.circular(2000),
                            ),
                          ),
                          child: Center(
                              child: Text(
                            CartRepository.differentProductsInCart().toString(),
                            style: TextStyle(color: Colors.white, fontSize: 10),
                          )),
                        ),
                      )
                    : Container(
                        width: 1,
                        height: 1,
                      ),
              ],
            ),
            title: Text('Корзина'),
          ),
        ],
        currentIndex: _selectedIndex,
        unselectedItemColor: Colors.grey[400],
        selectedItemColor: Colors.red[600],
        onTap: (index) {
          MainNavigator.navigateToIndex(index);
        },
      ),
    );
  }
}
